#include <config.h>
#include <applet-widget.h>
#include <libgnomeui/gnome-window-icon.h>
#include <assert.h>

#include <libreef/libreef.h>
#include <libgnomevfs/gnome-vfs.h>

/* callbacks */
static void applet_set_title                 (const char       *title,
		                            gpointer         data);
static void applet_open_location             (const char       *uri,
		                            gboolean         new_window,
		                            gpointer         data);
static void applet_report_status             (const char       *uri,
		                            gpointer         data);
static void applet_report_load               (gboolean         complete,
		                            gpointer         data);
     
static ReefCallbacks callbacks = {
	&applet_set_title,
	&applet_open_location,
	&applet_report_status,
	&applet_report_load
};

static void applet_set_title                 (const char       *title,
					    gpointer         data)
{
	g_print ("APPLET: title `%s'\n", title);
}


static void applet_open_location             (const char       *uri,
		                            gboolean         new_window,
					    gpointer         data)
{
	if (new_window) {
		g_print ("APPLET: open `%s'\n", uri);
	} else {
		g_print ("APPLET: open `%s' (new window)\n", uri);
	}
}

static void applet_report_status             (const char       *status,
					    gpointer         data)
{
	g_print ("APPLET: status `%s'\n", status);
}

static void applet_report_load               (gboolean         complete,
					    gpointer         data)
{
	if (complete) {
		g_print ("APPLET: load complete\n");
	} else {
		g_print ("APPLET: load incomplete\n");
	}
}
     

static void
about (AppletWidget *applet, gpointer data)
{
	static const char *authors[] = { "Ian McKellar", NULL };
	static GtkWidget *about_box = NULL;

	if (about_box != NULL)
	{
		gdk_window_show(about_box->window);
		gdk_window_raise(about_box->window);
		return;
	}
	about_box = gnome_about_new (_("Reef Panel Applet"),
				     VERSION,
				     _("Copyright 2001 (C) Eazel Inc."),
				     authors,
				     "FIXME",
				     NULL);
	gtk_signal_connect( GTK_OBJECT(about_box), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed), &about_box );

	gtk_widget_show(about_box);
	return;
	applet = NULL;
	data = NULL;
}

static void
change_pixel_size(GtkWidget *w, int size, gpointer data)
{
	return;
}

static void
help_cb (AppletWidget *applet, gpointer data)
{
    GnomeHelpMenuEntry help_entry = { "reef-panel-applet", "index.html"};
    gnome_help_display(NULL, &help_entry);
}

int
main (int argc, char **argv)
{
	GtkWidget *applet;
	GtkWidget *container;
	ReefEnvironment *environment;
	ReefSVB *svb;
	int size;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gtk_init (&argc, &argv);
	gnome_vfs_init ();

	container = gtk_event_box_new ();
	gtk_widget_show (container);

	if (argc != 2) {
		g_print ("%s <svb uri>\n", argv[0]);
		return 1;
	}

	applet_widget_init ("reef-panel-applet", VERSION, argc,
			    argv, NULL, 0, NULL);
	gnome_window_icon_set_default_from_file 
		(GNOME_ICONDIR"/gnome-networktool.png");

	applet = applet_widget_new ("reef-panel-applet");
	if (!applet) {
		g_error (_("Can't create reef applet!"));
	}

	size = applet_widget_get_panel_pixel_size(APPLET_WIDGET(applet));

	applet_widget_add (APPLET_WIDGET (applet), container);
	svb = reef_svb_download (argv[1]);

	environment = reef_environment_new (GTK_EVENT_BOX (container), svb,
			&callbacks, NULL);


	/* here it is ok to connect here, this is because we have already
	 * gotten the size before, and thus don't care about the initial
	 * signal */
	gtk_signal_connect(GTK_OBJECT(applet),"change_pixel_size",
			   GTK_SIGNAL_FUNC(change_pixel_size),
			   NULL);

	gtk_widget_show (applet);

#if 0
	applet_widget_register_callback (APPLET_WIDGET (applet),
					 "scramble",
					 _("Scramble pieces"),
					 scramble,
					 fifteen);
#endif
	applet_widget_register_stock_callback (APPLET_WIDGET (applet),
					       "help",
					       GNOME_STOCK_PIXMAP_HELP,
					       _("Help"), help_cb, NULL);

	applet_widget_register_stock_callback (APPLET_WIDGET (applet),
					       "about",
					       GNOME_STOCK_MENU_ABOUT,
					       _("About..."),
					       about,
					       NULL);

	applet_widget_gtk_main ();

	return 0;
}

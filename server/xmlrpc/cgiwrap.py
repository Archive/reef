## This is the CGI wrapper for the XML-RPC
#
# It will take care of decoding the incoming POST detecting whether it is
# compressed or not, and if it is, uncompress it and extract the full
# string before passing it on to the proper XML-RPC handler
#
# Copyright (C) 2000, 2001 Red Hat, Inc.
# This code is distributed under GPL version 2.
#
# Cristian Gafton <gafton@redhat.com>
#       - transport work, decoding/encoding in various formats,
#         Input and Output classes.
# Erik Troan <ewt@redhat.com>
#       - work around the bad limitations of stock httplib using
#         myHTTP* classes.
#
# $Id$

import os
import sys
import gzip
import zlib
import base64
import string
import StringIO
import httplib
import mimetools
import socket

# Constants
COMPRESS_LEVEL = 6

# ====================================================================
# Exception Handling
#

class myStringI:
    def __init__(self, sock):
	self.sock = sock

    def flush(self):
	return

    def close(self):
	return

    def isatty(self):
	return 0

    def getvalue(self):
	raise ValueError, "getValue function not implemented"

    def seek(self):
	raise ValueError, "seek function not implemented"

    def tell(self):
	raise ValueError, "tell function not implemented"

    def truncate(self):
	raise ValueError, "truncate function not implemented"

    def reset(self):
	raise ValueError, "reset function not implemented"

    def read(self, bytes = None):
	if bytes:
	    try:
		return self.sock.recv(bytes)
	    except (socket.sslerror, socket.error):
		return ""

	try:
	    buf = ""
	    str = self.sock.recv(16384)
	    while str:
		buf = buf + str
		str = self.sock.recv(16384)
	except (socket.sslerror, socket.error):
	    pass

	return buf

    def readline(self):
	str = ''
	ch = self.sock.recv(1)
	while (len(ch) and ch != '\n'):
	    str = str + ch
	    ch = self.sock.recv(1)
	return str

# stolen from the standard myHTTP object to letus get callbacks right
def sharedgetreply(self):
    self.file = myStringI(self.sock)
    line = self.file.readline()
    if self.debuglevel > 0:
        print 'reply:', `line`
    try:
	[ver, code, msg] = string.split(line, None, 2)
    except ValueError:
        try:
            [ver, code] = string.split(line, None, 1)
            msg = ""
        except ValueError:
            self.headers = None
            return -1, line, self.headers
    if ver[:5] != 'HTTP/':
	self.headers = None
	return -1, line, self.headers
    errcode = string.atoi(code)
    errmsg = string.strip(msg)
    self.headers = mimetools.Message(self.file, 0)
    return errcode, errmsg, self.headers
    
class myHTTP(httplib.HTTP): pass
#    def getreply(self):
#	return sharedgetreply(self)

#    def __init__(self, host, port):
#	httplib.HTTP.__init__(self, host, port)
    
class myHTTPS(httplib.HTTPS):
    def getreply(self):
	return sharedgetreply(self)

    # bleah, we dont have access to the headers created in for the Output class here...
    def __init__(self, host, port, cachain = None, proxyFor = None,username=None, password=None):
        self.proxyFor = proxyFor
        self.username = username
        self.password = password
	httplib.HTTPS.__init__(self, host, port, cachain)

    def sslTunnelConnect(self, host,sock, port = 0):
        self.sock = sock
        if self.debuglevel > 0:
            print 'via HTTP CONNECT request for:',self.proxyFor
        self.putrequest("CONNECT", self.proxyFor + ":" + "443")
        if self.username and self.password:
            userpass = "%s:%s" % (self.username, self.password)
            import base64
            enc_userpass = string.strip(base64.encodestring(userpass))
            self.putheader("Proxy-Authorization", " Basic %s" % enc_userpass)
        self.putheader("Host", host+":"+"443")
        self.endheaders()

        errcode, errmsg, headers = self.getreply()       

        if errcode != 200:
            raise socket.error, "unable to set up CONNECTION tunnel"
        

    def connect(self, host, port = 0):
        if not port:
            i = string.find(host, ":")
            if i >= 0:
                host, port = host[:i], host[i+1:]
                try:
                    port = string.atoi(port)
                except string.atoi_error:
                    raise socket.error, "nonnumeric port"
            if not port:
                port = httplib.HTTPS_PORT
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if self.debuglevel > 0:
                print 'connect:', (host, port)
            sock.connect(host, port)
            if self.proxyFor:
                self.sslTunnelConnect(host,sock,port)
                
            ssl = socket.ssl(sock, self.ca_chain)
            self.sock = httplib.FakeSocket(sock, ssl)
            

class ProtocolError:
    # indicates an HTTP protocol error
    def __init__(self, url, errcode, errmsg, headers):
	self.url = url
	self.errcode = errcode
	self.errmsg = errmsg
	self.headers = headers
    def __repr__(self):
	return (
	    "<ProtocolError for %s: %s %s>" %
	    (self.url, self.errcode, self.errmsg)
	    )

class NotImplemented:
    def __init__(self, transport = None, encoding = None):
        self.__transport = transport
        self.__encoding = encoding      
    def __repr__(self):
        data = "<%s instance at %X>\n" % (self.__class__.__name__, id(self))
        data = data +  "  Unknown decoder for (transport='%s', encoding='%s')" % \
               (self.__transport, self.__encoding)
        return data

class NotProcessed:
    def __repr__(self):
        data = "<%s instance at %X>\n" % (self.__class__.__name__, id(self))
        data = data +  "  There has been no data passed through the process() function"
        return data

class InputStream:
    def __init__(self, fd, length, name = "<unknown>"):
        self.fd = fd
        self.length = int(length)
        self.name = name
    def __repr__(self):
        return "Input data is a stream of %d bytes for file %s.\n" % (self.length, self.name)

    
# ===============================================================================
# Extended capabilities for transport
#
# We allow for the following possible headers:
#
# Content-Transfer-Encoding:
#       This header tells us how the POST data is encoded in what we read.
#       If it is not set, we assume plain text that can be passed along
#       without any other modification. If set, valid values are:
#       - binary : straight binary data
#       - base64 : will pass through base64 decoder to get the binary data
#
# Content-Encoding:
#       This header tells us what should we do with the binary data obtained
#       after acting on the Content-Transfer-Encoding header. Valid values:
#       - x-gzip : will need to pass through GNU gunzip-like to get plain
#                  text out
#       - x-zlib : this denotes the Python's own zlib bindings which are a
#                  datastream based on gzip, but not quite
#       - x-gpg : will need to pass through GPG to get out the text we want

# ===============================================================================
# Input class to automate reading the posting from the network
# Having to work with environment variables blows, though
class Input:
    def __init__(self, headers = None):
        self.transfer = None
        self.encoding = None
        self.type = None
        self.data = None        
        self.length = 0
        self.lang = "C"
        self.name = ""
        
        if not headers:
            # we need to get them from environment
            if os.environ.has_key("HTTP_CONTENT_TRANSFER_ENCODING"):
                self.transfer = string.lower(os.environ["HTTP_CONTENT_TRANSFER_ENCODING"])
            if os.environ.has_key("HTTP_CONTENT_ENCODING"):
                self.encoding = string.lower(os.environ["HTTP_CONTENT_ENCODING"])
            if os.environ.has_key("CONTENT-TYPE"):
                self.type = string.lower(os.environ["CONTENT-TYPE"])
            if os.environ.has_key("CONTENT_LENGTH"):
                self.length = int(os.environ["CONTENT_LENGTH"])
            if os.environ.has_key("HTTP_ACCEPT_LANGUAGE"):
                self.lang = os.environ["HTTP_ACCEPT_LANGUAGE"]
            if os.environ.has_key("HTTP_X_PACKAGE_FILENAME"):
                self.name = os.environ["HTTP_X_PACKAGE_FILENAME"]
        else:
            # The stupid httplib screws up the headers from the HTTP repsonse
            # and converts them to lowercase. This means that we have to
            # convert to lowercase all the dictionary keys in case somebody calls
            # us with sane values --gaftonc (actually mimetools is the culprit)
            for header in headers.keys():
                value = string.lower(headers[header])               
                h = string.lower(header)
                if h == "content-length":
                    self.length = value
                elif h == "content-transfer-encoding":
                    self.transfer = value
                elif h == "content-encoding":
                    self.encoding = value
                elif h == "content-type":
                    self.type = value
                elif h == "accept-language":
                    self.lang = headers[header]
                elif h == "x-package-filename":
                    self.name = headers[header]
            
    def read(self, fd = sys.stdin):
        # XXX:  we should look at the possibility of creating a temporary file and
        #       maybe keeping the submitted data in there until it gets passed to the
        #       XML-RPC layer - this could reduce dramatically the memory requirements.
        #       We can do this as a function of Content-Length:

        # The octet-streams are passed right back
        if self.type == "application/octet-stream":
            return
        
        self.data = fd.read()
        if not self.transfer:
            self.encoding = "__plain" # internal use only
            return
        elif self.transfer == "binary":
            return
        elif self.transfer == "base64":
            self.data = base64.decodestring(self.data)
        else:
            raise NotImplemented(self.transfer)

    def decode(self, fd = sys.stdin):
        # The octet-stream data are passed right back
        if self.type == "application/octet-stream":
            raise InputStream(fd, self.length, self.name)
        
        if not self.data:
            self.read(fd)
        # Now we have the binary goo
        if not self.encoding or self.encoding == "__plain":
            # all is fine
            return self.__getfd()
        elif self.encoding == "x-zlib":
            obj = zlib.decompressobj()
            self.data = obj.decompress(self.data) + obj.flush()
            del obj
            self.length = len(self.data)
        elif self.encoding == "x-gzip":
            f = StringIO.StringIO(self.data)
            gz = gzip.GzipFile(mode="rb", compresslevel = COMPRESS_LEVEL,
                               fileobj = f)
            self.data = gz.read()
            self.length = len(self.data)
            f.close()

        elif self.encoding == "x-gpg":           
            # XXX: should be written
            raise NotImplemented(self.transport, self.encoding)
        else:
            raise NotImplemented(self.transport, self.encoding)

        return self.__getfd()
    
    def __getfd(self):
        # XXX: maybe someday we will use a real fd here. For now, fake it.
        fd = StringIO.StringIO(self.data)
        fd.seek(0)
        return fd

    def getlang(self):
        return self.lang
    
# ===============================================================================
# Output class that will be used to build the temporary output string
class Output:
    # DEFINES for instances use   
    # Content-Encoding
    ENCODE_NONE = 0
    ENCODE_GZIP = 1
    ENCODE_ZLIB = 2
    ENCODE_GPG  = 3
    
    # Content-Transfer-Encoding
    TRANSFER_NONE   = 0
    TRANSFER_BINARY = 1
    TRANSFER_BASE64 = 2

    def __init__(self, transfer = 0, encoding = 0):
        self.data = None
        self.headers = {}
        self.encoding = encoding
        self.transfer = transfer
        self.length = -1
        # for authenticated proxies
        self.username = None
        self.password = None
        # internal flags
        self.__processed = 0
        self.__http = None
        self.__host = None
        self.__handler = None
        self.__headers = None
        self.__ca_chain = None
        
    def add_header(self, name, arg):
        # KISS
        self.headers[name] = str(arg)
        
    def process(self, data):
        # Assume straight text/xml
        self.data = data

        # Content-Encoding header
        if self.encoding == self.ENCODE_GZIP:
            self.add_header("Content-Encoding", "x-gzip")
            f = StringIO.StringIO()
            gz = gzip.GzipFile(mode="wb", compresslevel=COMPRESS_LEVEL,
                               fileobj = f)
            gz.write(data)
            gz.close()
            self.data = f.getvalue()
            f.close()
        elif self.encoding == self.ENCODE_ZLIB:
            self.add_header("Content-Encoding", "x-zlib")
            obj = zlib.compressobj(COMPRESS_LEVEL)
            self.data = obj.compress(data) + obj.flush()
        elif self.encoding == self.ENCODE_GPG:
            # XXX: fix me.
            raise NotImplemented(self.transfer, self.encoding)
            self.add_header("Content-Encoding", "x-gpg")

        # Content-Transfer-Encoding header
        if self.transfer == self.TRANSFER_BINARY:
            self.add_header("Content-Transfer-Encoding", "binary")
            self.add_header("Content-Type", "application/binary")
        elif self.transfer == self.TRANSFER_BASE64:
            self.add_header("Content-Transfer-Encoding", "base64")
            self.add_header("Content-Type", "text/base64")
            self.data = base64.encodestring(self.data)
            
        # other headers
        self.add_header("Content-Length", len(self.data))
        self.add_header("X-Info", 'XML-RPC Processor (C) Red Hat, Inc ($Revision$)')
        # identify the capability set of this client to the server
        self.add_header("X-Client-Version", 1)        
        self.__processed = 1
        
    def get_strings(self):
        if not self.__processed:
            raise NotProcessed
        h = StringIO.StringIO()       

        for header in self.headers.keys():
            h.write("%s: %s\r\n" % (header, self.headers[header]))
        tmp = h.getvalue()
        h.close()
        # return the headers and the body
        return tmp, self.data
        
    # reset the transport options
    def set_transport(self, transfer = 0, encoding = 0):
        self.transfer = transfer
        self.encoding = encoding
    
    def use_CA_chain(self, ca_chain):
        if not ca_chain:
            return 0
        if not os.access(ca_chain, os.R_OK):
            return 0
        self.__ca_chain = ca_chain
        return 1
    
    # XXX: we assume that port = 0 is the defualt in httplib.
    def send_http(self, host, proxy = None, handler = "/RPC2",
                  http_type = "http", port = 0):
        if not self.__processed:
            raise NotProcessed

        # make sure we're not discriminating against case
        ht = string.lower(http_type)
        if proxy:
            if ht == "http":
                h = myHTTP(proxy, port)
            elif ht == "https":
                h = myHTTPS(proxy, port, self.__ca_chain,
                            host, username = self.username,
                            password = self.password)
        else:
            if ht == "http":
                h = myHTTP(host, port)
            elif ht == "https":
                h = myHTTPS(host, port, self.__ca_chain)
            
	h.putrequest("POST", handler)
        
	# required by HTTP/1.1
	h.putheader("Host", host)

        # all the other headers
        for header in self.headers.keys():
            h.putheader(header, self.headers[header])
        
	h.endheaders()
        
	if self.data:
	    h.send(self.data)
        
        self.__http = h

        errcode, errmsg, headers = h.getreply()       

	if errcode != 200:
            if proxy:
                raise ProtocolError(proxy + " " + handler, errcode,
                                    errmsg, headers)
            else:
                raise ProtocolError(host + " " + handler, errcode,
                                    errmsg, headers)
                
        # Multiple returns suck, but hey, what's a man gonna do?
        return headers.dict, h.getfile()

    def close(self):
        self.__http.close()

#
# Main program
#

if __name__ == '__main__':
    print "$Id$"
    print "Not meant to be used as a python program"
    

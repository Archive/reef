import SocketServer, BaseHTTPServer
import xmlrpcserver


server = SocketServer.TCPServer(('', 8889), xmlrpcserver.RequestHandler)
server.serve_forever()

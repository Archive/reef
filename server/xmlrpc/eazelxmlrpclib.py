import xmlrpclib
import string
import commands
import types

#########################

class EazelServer:
    
    serverMappingDefault = None
    serverMapping = {}

    def __init__(self, uri=None, proxy = None, transport = None,
                 refreshCallback = None, username=None, password=None):
        self.__uri = uri
        self.__proxy = proxy
        self.__transport = transport
        self.__refreshCallback = refreshCallback
        self.__username = username
        self.__password = password
        self.__server_uri = None
        self.__server = None  # defer instantiation of xmlrpc.Server

    def __request(self, methodname, params):

        # if a uri was given with init(), then that prevails: old xmlrpclib.Server behaviour 
        if not self.__uri:
            uri = getServerMapping(methodname)
        else:
            uri = self.__uri
            
        if not isinstance(self.__server, xmlrpclib.Server) or self.__server_uri != uri:
            self.__server = xmlrpclib.Server(uri, proxy=self.__proxy, transport=self.__transport, refreshCallback=self.__refreshCallback, username=self.__username, password=self.__password)
            self.__server_uri = uri
            
        return self.__server._Server__request(methodname, params)
    
    def __repr__(self):
        if isinstance(self.__server, xmlrpclib.Server):
            return self.__server.__repr__()
        else:
            return "<xmlrpc-server unknown yet>"
    
    __str__ = __repr__


    def __getattr__(self, name):
	# magic method dispatcher
	return xmlrpclib._Method(self.__request, name)
    

#########################

class EazelClassServer(EazelServer):
    """Prepends the "module.class" to the method.
    This facilitates a use of xmlrpc invocation where the complete path of "module.class.method" is used to define the namespace for the RPCs.
    """
    
    def __request(self, methodname, params):

        # change the method name in a "class.method" invocation
        methodname = self.__class__.__module__ + "." + self.__class__.__name__ + "." + methodname
        # note the _Server that is used to "hide" the attributes with __ prepended
        return EazelServer._EazelServer__request(self, methodname, params)


    def __getattr__(self, name):
        # magic method dispatcher
        # we seem to have to copy this over from EazelServer, otherwise the local __request doesn't get called!
        return xmlrpclib._Method(self.__request, name)


#########################

def getDottedClassMethod(classMethod):
    """Returns the "classMethod" parameter as a dotted string. If "classMethod" already is a string, it is simply returned. If classMethod is a class object, the 'module.class' string is returned. I classMethod is a method (bound or unbound), then the 'module.class.method' string is returned. If anything else, an empty string will be the result.
    """
    
    if isinstance(classMethod, types.StringType):
        # just return the string
        return classMethod
    elif isinstance(classMethod, types.ClassType):
        # do class thing
        return classMethod.__module__ + "." + classMethod.__name__
    elif isinstance(classMethod, types.MethodType) or isinstance(classMethod, types.UnboundMethodType):
        # do the method thing
        return classMethod.im_class.__module__ + "." + classMethod.im_class.__name__ + "." + classMethod.__name__
    else:
        # give up
        return ""


#########################

def setServerMapping(mappedUri, classMethod=None):
    """Add a tuple with "dotted" class.method string and associated uri to the "serverMapping" dictionary.
    If "classMethod is omitted, a process-wide default is set for the server uri.
    Note that "mappedUri" is not checked for validity"
    """
    
    dottedClassMethod = getDottedClassMethod(classMethod)
    EazelServer.serverMapping[dottedClassMethod] = mappedUri
    
        
#########################

def getServerMapping(classMethod=None):
    """Transforms the "classMethod" parameter into a dotted string with "getDottedClassMethod()", and then looks for the most specific match in the "serverMapping" dictionary and returns the associated uri.
    "Most specific match" means that for "a.b.c", first the dictionary is search for the key "a.b.c", then for "a.b" and lastly for "a". This allows you to register a server for a top level path, and over write that mapping for a more specific path.
    If the classMethod is None, empty, or not known, the default mapping is return, which is the one for key "".
    If there isn't any default mapping either, then 'None' is returned.
    """
    
    dottedClassMethod = getDottedClassMethod(classMethod)
    for k in getMappingList(dottedClassMethod):
        if EazelServer.serverMapping.has_key(k):
            return EazelServer.serverMapping[k]
    if EazelServer.serverMapping.has_key(""):
        return EazelServer.serverMapping[""]
    else:
        return None
    

#########################

def getMappingList(stringWithDots):

    """Given a dotted string, like "a.b.c", a list is returned of decreasing namespace specificity: ["a.b.c", "a.b", "a"]. This is used for iterating to find the most specific mapped server URIs in "getServerMapping".
    """
    
    mappingList = [stringWithDots]
    end = string.rfind(stringWithDots, ".", 0, len(stringWithDots))
    while (end >= 0):
        mappingList.append(stringWithDots[0:end])
        end = string.rfind(stringWithDots, ".", 0, end)
        
    # debug(1, mappingList)

    return mappingList

#########################
    

if __name__ == "__main__":

    # simple test program (from the XML-RPC specification)
    # server = Server("http://localhost:8000") # local server

    server = xmlrpclib.Server("http://localhost:8888")

    server2 = xmlrpclib.Server("http://betty.userland.com")

    print server

    try:
	print server.examples.getStateName(5)
    except xmlrpclib.Error, v:
	print "ERROR", v

import eazelxmlrpclib
import mycompute


eazelxmlrpclib.setServerMapping("http://localhost:8000/None")

eazelxmlrpclib.setServerMapping("http://localhost:8000/a.b.c", "a.b.c")

eazelxmlrpclib.setServerMapping("http://localhost:8000/a.b", "a.b")

eazelxmlrpclib.setServerMapping("http://localhost:8000/a", "a")

eazelxmlrpclib.setServerMapping("http://localhost:8000/mycompute.computeSvc", mycompute.computeSvc)

eazelxmlrpclib.setServerMapping("http://localhost:8000/mycompute.myIntImpl", mycompute.myIntImpl)

m = mycompute.myIntImpl()
eazelxmlrpclib.setServerMapping("http://localhost:8000/m.add", m.add)

print eazelxmlrpclib.EazelServer.serverMapping

print eazelxmlrpclib.getServerMapping('mycompute.myIntImpl.add')
print eazelxmlrpclib.getServerMapping('mycompute.myIntImpl.minus')
print eazelxmlrpclib.getServerMapping('mycompute.computeSvc')
print eazelxmlrpclib.getServerMapping('a.b.c')
print eazelxmlrpclib.getServerMapping('a.b')
print eazelxmlrpclib.getServerMapping('a.b.d')
print eazelxmlrpclib.getServerMapping('a.b.c.e')
print eazelxmlrpclib.getServerMapping('')
print eazelxmlrpclib.getServerMapping()
print eazelxmlrpclib.getServerMapping({})



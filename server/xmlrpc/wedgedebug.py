import types

wedgedfuns = {}
origfuns = {}
indent = 0

def wedgedebug(fun):
    """ Takes a callable user-defined function or method, and returns a new function that can be assigned to that same function/method. This new function will call the original function and will also print out debugging info about the parameters and return values.
    """
    global wedgedfuns, origfuns
    if isinstance(fun, types.FunctionType) or \
       isinstance(fun, types.MethodType) or \
       isinstance(fun, types.UnboundMethodType) :
        if isinstance(fun, types.MethodType) or \
           isinstance(fun, types.UnboundMethodType) :
            if wedgedfuns.values().count(fun.im_func) > 0 :
                print "function already wedged..."
                return fun
            funname = fun.im_class.__module__ + "." + fun.im_class.__name__ + "." + fun.__name__
        else:
            if wedgedfuns.values().count(fun) > 0 :
                print "function already wedged..."
                return fun
            funname = fun.func_globals["__name__"] + "." + fun.__name__
        origfuns[funname] = fun
        m = "lambda *args, **keywords : myapply('" + funname + "', args, keywords)"
        wedgedfun = eval(m)
        wedgedfuns[funname] = wedgedfun
        ## fun = wedgedfun
        return wedgedfun
    else:
        print "sorry...not callable type"
        return fun

def myapply(funname, args, keywords):
    """This is the 'wedge' function that calls the original function after printing the parameters before and return value after
    """
    global wedgedfuns, indent
    if origfuns.has_key(funname):
        f = origfuns[funname]
        print indent*" " + "<" + str(funname) + ":in> " + str(args) + str(keywords)
        if isinstance(args[0], types.InstanceType):
            # if first agument is an instance, then we're in a class method and can look at the instance itself in more detail
            print "<" + args[0].__class__.__module__ + "." + args[0].__class__.__name__ + "-in>", args[0].__dict__
        indent = indent + 4
        ret = apply(f, args, keywords)
        indent = indent - 4
        if isinstance(args[0], types.InstanceType):
            print "<" + args[0].__class__.__module__ + "." + args[0].__class__.__name__ + "-out>", args[0].__dict__
        print indent*" " + "<" + str(funname) + ":out> " + str(ret)
        return ret
    else:
        print "function:" + funname + " doesn't seem to be wedged..."
        

def unwedgedebug(wfun):
    """ Takes a wedged function or method, and returns the original.By assigning it to the same function/method, the debugging wedge is removed and the original function restored
    """
    global wedgedfuns, origfuns
    if isinstance(wfun, types.FunctionType) or \
       isinstance(wfun, types.MethodType) or \
       isinstance(wfun, types.UnboundMethodType) :
        if isinstance(wfun, types.MethodType) or \
           isinstance(wfun, types.UnboundMethodType):
            fim = wfun.im_func
        else:
            fim = wfun
        if wedgedfuns.values().count(fim) == 0 :
            print "function/method doesn't seem wedged..."
            return wfun
        else:
            for k in wedgedfuns.keys():
                if wedgedfuns[k] == fim:
                    return origfuns[k]
            print "can't find wedged function - shouldn't get here....????"
            return wfun
    else:
        print "sorry...not callable type"
        return wfun



if __name__ == "__main__":

    # simple test program to show how the "wedging" works for normal user-defined function

    def tst(p1, p2="hallo"):
        print p1, p2
        return "returning..."

    print "function tst: see if we get what we expect"
    tst("hello")
    tst("ja", "nee")

    print "now replace the user-defined function with the wedged one"
    tst = wedgedebug(tst)
    print "should show debugging info about parameters and return values"
    tst("hello")
    tst("ja", "nee")
    
    print "testing double wedging protection"
    tst = wedgedebug(tst)
    print "all still works..."
    tst("hello")
    tst("ja", "nee")


    print "class Tst: a simple class.method example"

    class Tst:
        def tst(self, p1, p2="hallo"):
            if not self.__dict__.has_key("jaja"): self.jaja = ""
            self.jaja = self.jaja + "nee"
            print p1, p2
            return "returning..."
        
    print "see if we get what we expect"
    t = Tst()
    t.tst("hello")
    t.tst("ja", "nee")

    print "now replace the method function with the wedged one"
    Tst.tst = wedgedebug(Tst.tst)
    # should show debugging info about parameters and return values
    t.tst("hello")
    t.tst("ja", "nee")

    print "testing double wedging protection"
    Tst.tst = wedgedebug(Tst.tst)
    # all still works...
    t.tst("hello")
    t.tst("ja", "nee")


    print "unwedging tests"

    tst = unwedgedebug(tst)
    # see if we get what we expect
    tst("hello")
    tst("ja", "nee")
    tst = unwedgedebug(tst)
    
    Tst.tst = unwedgedebug(Tst.tst)
    # see if we get what we expect
    t.tst("hello")
    t.tst("ja", "nee")
    Tst.tst = unwedgedebug(Tst.tst)

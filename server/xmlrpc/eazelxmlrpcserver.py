import SocketServer, BaseHTTPServer
import string
import xmlrpcserver
import commands
import os
import eazelxmlrpclib

#########################

class EazelRequestHandler(xmlrpcserver.RequestHandler):

    realHandlers = {}

    def call(self, method, params):
        
        print method, params
        methodParts = string.split(method, ".")

        # print eazelxmlrpclib.getPeersLocalhostUid(self.client_address[1], self.server.fileno())
        
        realHandler = getRpcHandlerMapping(method)
        if realHandler:
            ## get the callable object for the handler, i.e. the method itself
            func = eval(realHandler.__class__.__name__ + "." + methodParts[len(methodParts)-1])
            paramlist = list(params)
            ## insert the instance as the first parameter
            paramlist.insert(0, realHandler)
            ## now finally call the class methos on the instance with the parameters
            return apply(func, paramlist)
        else:
            callstr = "self." + method + "("
            for p in range(len(params)):
                if p != 0:
                    callstr = callstr + ", "
                callstr = callstr + "params[" + str(p) + "]"
            callstr = callstr + ")"
            print callstr
            return eval(callstr)

#########################

def setRpcHandlerMapping(mappedImplClass, classMethod=None):
    """Add a tuple with 'dotted' class.method string and associated server implementor class to the 'realHandlers' dictionary.
    If 'classMethod' is omitted, a process-wide default is set for the server implementor class.
    """
    
    dottedClassMethod = eazelxmlrpclib.getDottedClassMethod(classMethod)
    EazelRequestHandler.realHandlers[dottedClassMethod] = mappedImplClass
    

                   
#########################

def getRpcHandlerMapping(classMethod=None):
    """Transforms the "classMethod" parameter into a dotted string with "getDottedClassMethod()", and then looks for the most specific match in the "realHandlers" dictionary and returns the associated implementor class.
    "Most specific match" means that for "a.b.c", first the dictionary is search for the key "a.b.c", then for "a.b" and lastly for "a". This allows you to register a server for a top level path, and over write that mapping for a more specific path.
    If the classMethod is None, empty, or not known, the default mapping is return, which is the one for key "".
    If there isn't any default mapping either, then 'None' is returned.
    """
    
    dottedClassMethod = eazelxmlrpclib.getDottedClassMethod(classMethod)
    for k in eazelxmlrpclib.getMappingList(dottedClassMethod):
        if EazelRequestHandler.realHandlers.has_key(k):
            return EazelRequestHandler.realHandlers[k]
    if EazelRequestHandler.realHandlers.has_key(""):
        return EazelRequestHandler.realHandlers[""]
    else:
        return None
 




#########################

if __name__ == '__main__':

    class Plus:
        def add(self, a, b):
            return (a+b)

    
    ppp = Plus()
    
    setRpcHandlerMapping(ppp,"plus")
    setRpcHandlerMapping(ppp,"ComputeSvc")
    setRpcHandlerMapping(ppp, "__main__.ComputeSvc")

    server = SocketServer.TCPServer(('', 8888), EazelRequestHandler)
    server.serve_forever()

#!/usr/bin/perl

$ROOT=$ARGV[0] || die "must specify a root directory";
@FILES=split("\n", `find $ROOT -type f`);

print "Running fixconfig....\n";

foreach $f (@FILES) {
   if (-T $f and `grep -c "/var/tmp" $f` > 0) {
      print "$f...\n";
      open (F, $f);
      undef $/;
      $lines = <F>;
      close (F);
      $lines =~ s/\/var\/tmp\/apache_[0-9.]*-root//g;
      # add hack for apachectl so it picks up correct httpd.conf file
      if (index($f, "apachectl") > 0) {
         $lines =~ s/(HTTPD=)(\/gnome\/etc\/reef\/httpd\/bin\/httpd)/$1"$2 -f \/gnome\/etc\/reef\/httpd\/conf\/httpd.conf"/g;
      }
      open (F, ">$f");
      print F $lines;
      # add httpd.conf file so that it loads md5_compare module
      if (index($f, "httpd.conf") > 0) {
         print F "\n\n";
         print F "LoadModule md5_compare_module libexec/mod_md5_compare.so\n";
         print F "AddType application/x-reef-svb .svb\n";
         print F "AddHandler md5-compare-handler .svb\n";
      }
      close (F);
   }
}

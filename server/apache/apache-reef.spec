#
# ----------------------------------------------------------------------------- 
# apache-reef.spec
# ----------------------------------------------------------------------------- 
#
%define ver 1.3.19
%define RELEASE reef_11
%define rel %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define dest /gnome/etc/reef/httpd
%define src /gnome-source/reef/server/apache

Summary: A web server configured for the reef development
Name: apache
Version: %ver
Release: %rel
Copyright: Apache license
Group: System Environment/Daemons
Source: http://www.apache.org/dist/apache_%{ver}.tar.gz
BuildRoot: /var/tmp/apache_%{ver}-root
Provides: reef-devel-webserver
#
# ----------------------------------------------------------------------------- 
# description
# ----------------------------------------------------------------------------- 
#
%description
Standard Apache HTTPD Server distribution which has been built for use
in serving reef svb's.
#
# ----------------------------------------------------------------------------- 
# prep
# ----------------------------------------------------------------------------- 
#
%prep

BUILDTGZ=%{name}_%{version}.tar.gz
PRE_PATCH=apache-reef-patch

mkdir -p $RPM_BUILD_ROOT
cp $BUILDTGZ $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT
tar xfvz $BUILDTGZ

#
# Apply pre-patch
#
cp %{src}/$PRE_PATCH $RPM_BUILD_ROOT/%{name}_%{version}
cd $RPM_BUILD_ROOT/%{name}_%{version}
patch -p1 <$PRE_PATCH

#
# Add modules to apache
#
APXS=`which apxs`
MODULE_NAMES='md5_compare'
MODULE_DIRECTIVES=
for MODULE_NAME in $MODULE_NAMES; do
	MODULE_DIR=src/modules/$MODULE_NAME
	MODULE_SRC=mod_$MODULE_NAME.c
	MODULE_DIRECTIVES="$MODULE_DIRECTIVES --add-module=$MODULE_DIR/$MODULE_SRC --enable-shared=$MODULE_NAME"
	mkdir -p $MODULE_DIR
	cp %{src}/$MODULE_SRC $MODULE_DIR
done

#
# configure apache
#
cd $RPM_BUILD_ROOT/%{name}_%{version}
./configure	\
--enable-module=so \
--prefix=$RPM_BUILD_ROOT%{dest} \
--bindir=$RPM_BUILD_ROOT%{dest}/bin \
--sbindir=$RPM_BUILD_ROOT%{dest}/bin \
--libexecdir=$RPM_BUILD_ROOT%{dest}/libexec \
--mandir=$RPM_BUILD_ROOT%{dest}/man \
--sysconfdir=$RPM_BUILD_ROOT%{dest}/conf \
--datadir=$RPM_BUILD_ROOT%{dest}/htdocs \
--iconsdir=$RPM_BUILD_ROOT%{dest}/icons \
--htdocsdir=$RPM_BUILD_ROOT%{dest}/htdocs \
--cgidir=$RPM_BUILD_ROOT%{dest}/cgi-bin \
--includedir=$RPM_BUILD_ROOT%{dest}/include \
--localstatedir=$RPM_BUILD_ROOT%{dest}/var \
--runtimedir=$RPM_BUILD_ROOT%{dest}/logs \
--logfiledir=$RPM_BUILD_ROOT%{dest}/logs \
--proxycachedir=$RPM_BUILD_ROOT%{dest}/logs \
--with-port=7123 \
$MODULE_DIRECTIVES
#
# ----------------------------------------------------------------------------- 
# build
# ----------------------------------------------------------------------------- 
#
%build
cd $RPM_BUILD_ROOT/%{name}_%{version}
make
#
# ----------------------------------------------------------------------------- 
# install
# ----------------------------------------------------------------------------- 
#
%install
cd $RPM_BUILD_ROOT/%{name}_%{version}
make install
FIXCONFIG=%{src}/fixconfig.pl 
$FIXCONFIG $RPM_BUILD_ROOT%{dest}
#
# ----------------------------------------------------------------------------- 
# clean
# ----------------------------------------------------------------------------- 
#
%clean
rm -rf $RPM_BUILD_ROOT
#
# ----------------------------------------------------------------------------- 
# files
# ----------------------------------------------------------------------------- 
#
%files
%defattr(-,root,root)
%{dest}
#
# ----------------------------------------------------------------------------- 
# changelog
# ----------------------------------------------------------------------------- 
#
%changelog
* Mon Mar 23 2001 Kenneth Kocienda <kocienda@eazel.com> 
- First hack.
#
# ----------------------------------------------------------------------------- 
# end
# ----------------------------------------------------------------------------- 
#


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2001 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * 
 * Author: Ken Kocienda <kocienda@eazel.com>
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "httpd.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "util_md5.h"

#define M_HEAD "HEAD"
#define MD5_HEADER "MD5"
#define MD5_SUM_LENGTH 33
#define CACHE_DEFAULT_LOAD 0.75f
#define SVB_CONTENT_TYPE "application/x-reef-svb"


/* the MD5CacheNode struct */
typedef struct md5_cache_node {
    unsigned int hashcode;
    char *filename;
    time_t mtime;
    char md[MD5_SUM_LENGTH];
    struct md5_cache_node *next;
} MD5CacheNode;

/* the MD5Cache struct */
typedef struct md5_cache_struct {
    unsigned int size;
    unsigned int cap;
    int threshold;
    float load;
    struct md5_cache_node **table;
} MD5Cache;

static MD5Cache *md5_cache;

/* constants */
enum {
    /* the factor by which the alloc list grows */
    CACHE_RESIZE_FACTOR = 2,
    /* the initial capacity of the alloc list */
    CACHE_DEFAULT_SIZE  = 37,   
};

extern int errno;

module md5_compare_module;

/* whether or not to use the module in a particular directory */
typedef struct {
    char *directory;
    int active;
} MD5CompareConfig;


static void md5_cache_init(server_rec *server, pool *p);
static void md5_cache_resize();
static void md5_cache_add(server_rec *server, char *filename, time_t mtime, char *md);
static MD5CacheNode *md5_cache_get(char *filename);
static void md5_cache_resize(server_rec *server);

unsigned int compute_hash_code(const char *s);
static void *emalloc(server_rec *server, size_t size);

static void *emalloc(server_rec *server, size_t size) {
    void *p;

    p = malloc(size);
    if (p == NULL) {
        ap_log_error(APLOG_MARK, APLOG_CRIT, server, "memory allocation failure");
        exit(1);
    }

    return p;
}

unsigned int compute_hash_code(const char *s) {
   unsigned int h = 0;
   int i = 0;
   
   while (s[i] != '\0') {
      h = h * 37 + s[i];
      i++;
   }
   
   return h;
}

static void module_init(server_rec *server, pool *p) {
    md5_cache_init(server, p);
}


static void md5_cache_init(server_rec *server, pool *p) {
    int i;
    int cap;

    md5_cache = emalloc(server, sizeof(MD5Cache));

    /* set other fields */
    cap = md5_cache->cap = CACHE_DEFAULT_SIZE;
    md5_cache->size = 0;
    md5_cache->load = CACHE_DEFAULT_LOAD;
    md5_cache->threshold = ((float)cap * md5_cache->load);

    md5_cache->table = emalloc(server, cap * sizeof(MD5CacheNode *));

    /* init table */
    for (i = 0; i < cap; i++) {
        md5_cache->table[i] = NULL;
    }
}

static void md5_cache_add(server_rec *server, char *filename, time_t mtime, char *md) {
    int already_exist;
    unsigned int hashcode;
    unsigned int index;
    MD5CacheNode *node;

    already_exist = 0;
    hashcode = compute_hash_code(filename);
    index = hashcode % (md5_cache->cap);

    for (node = md5_cache->table[index]; node != NULL; node = node->next) {
        if (hashcode == node->hashcode && strcmp(filename, node->filename) == 0) {
            node->mtime = mtime;
            strcpy(node->md, md);
            already_exist = 1;
            break;
        }
    }

    if (already_exist == 0) {
        if (md5_cache->size >= md5_cache->threshold) {
            md5_cache_resize(server);
        }
        index = hashcode % (md5_cache->cap);
        node = (MD5CacheNode *)emalloc(server, sizeof(MD5CacheNode));   
        node->hashcode = hashcode;
        node->filename = emalloc(server, strlen(filename) + 1);
        strcpy(node->filename, filename);
        node->mtime = mtime;
        strcpy(node->md, md);
        node->next = md5_cache->table[index];   
        md5_cache->table[index] = node;
    }
}

static MD5CacheNode *md5_cache_get(char *filename) {
    void *result;
    unsigned int hashcode;
    unsigned int index;
    MD5CacheNode *node;

    result = NULL;
    hashcode = compute_hash_code(filename);
    index = hashcode % (md5_cache->cap);
    node = md5_cache->table[index];

    while (node != NULL) {
        if (hashcode == node->hashcode && strcmp(filename, node->filename) == 0) {
            result = node;
            break;
        }
        node = node->next;
    }

    return result;   
}

static void md5_cache_resize(server_rec *server) {
    int i;
    unsigned int index;
    unsigned int oldcap;
    unsigned int newcap;
    MD5CacheNode **oldtable;
    MD5CacheNode **newtable;
    MD5CacheNode *old;

    oldcap = md5_cache->cap;
    oldtable = md5_cache->table;
    newcap = (md5_cache->cap * CACHE_RESIZE_FACTOR) + 1;

    newtable = emalloc(server, newcap * sizeof(MD5CacheNode *));

    /* init bucket stuff */
    for (i = 0; i < newcap; i++) {
        newtable[i] = NULL;   
    }

    for (i = oldcap; i-- > 0; ) {
        for (old = oldtable[i]; old != NULL; ) {
            MD5CacheNode *node = old;
            old = old->next;
            index = (node->hashcode) % newcap;
            node->next = newtable[index];
            newtable[index] = node;
        }
    }

    md5_cache->table = newtable;

    free(oldtable);

    md5_cache->cap = newcap;
    md5_cache->threshold = ((float)newcap * md5_cache->load);
}


static const char *cmd_md5_compare(cmd_parms *cmd, void *mconfig)
{
    MD5CompareConfig *config = (MD5CompareConfig *) mconfig;

    config->active = 1;
    return NULL;
}

/* create per-dir config structure */
static void *create_dir_config(pool *p, char *dirspec)
{
    MD5CompareConfig *config;

    config = (MD5CompareConfig *)ap_pcalloc(p, sizeof (MD5CompareConfig));
    config->active = 0;
    config->directory = ap_pstrdup(p, dirspec ? dirspec : "/");
    return(void *)config;
}

/* create per-server config structure */
static void *create_server_config (pool *p, server_rec *s)
{
    MD5CompareConfig *config;

    config = (MD5CompareConfig *)ap_pcalloc(p, sizeof (MD5CompareConfig));
    config->active = 0;
    config->directory = ap_pstrdup (p, "(server)");
    return(void *)config;
}

/* merge per-dir (or per-server) config structure */
static void *merge_config (pool *p, void *parent_conf, void *newloc_conf)
{
    MD5CompareConfig *config;
    MD5CompareConfig *parent_config = (MD5CompareConfig *) parent_conf;
    MD5CompareConfig *child_config = (MD5CompareConfig *) newloc_conf;

    config = (MD5CompareConfig *)ap_pcalloc(p, sizeof (MD5CompareConfig));
    config->active = (parent_config->active | child_config->active);
    config->directory = ap_pstrdup(p, child_config->directory);
    return(void *)config;
}

static const command_rec md5_compare_cmds[] =
{
    {
        "MD5Compare",                  /* directive name */
        cmd_md5_compare,                /* config action routine */
        NULL,                   /* argument to include in call */
        OR_OPTIONS,             /* where available */
        NO_ARGS,                /* arguments */
        "MD5Compare - no arguments"    /* directive description */
    },
    {NULL}
};

static int md5_compare_handler(request_rec *r)
{
    struct stat buf;
    int stat_result;
    int result;
    char md[MD5_SUM_LENGTH];
    MD5CacheNode *node;

    result = OK;
    r->content_type = "text/html";

    ap_soft_timeout("md5_compare", r);

    stat_result = stat(r->filename, &buf);
    if (stat_result == -1) {
        switch(errno) {
            case ENOENT:
                result = HTTP_NOT_FOUND;
                break;
            case EACCES:
                result = HTTP_FORBIDDEN;
                break;
            default:
                result = HTTP_INTERNAL_SERVER_ERROR;
                break;
        }
    }
    else {
        node = md5_cache_get(r->filename);
        if (node != NULL && node->mtime == buf.st_mtime) {
            strcpy(md, node->md);
            ap_log_error(APLOG_MARK, APLOG_DEBUG, r->server, "cached md5 for %s -> %s", r->filename, md);
        }
        else {
            FILE *md5_file;

            md5_file = ap_pfopen(r->pool, r->filename, "r");

            if (md5_file == NULL) {
                result = HTTP_FORBIDDEN;
            }
            else {
                strcpy(md, ap_md5digest(r->pool, md5_file));

                ap_pfclose(r->pool, md5_file);

                md5_cache_add(r->server, r->filename, buf.st_mtime, md);

                ap_log_error(APLOG_MARK, APLOG_DEBUG, r->server, "computed md5 for %s -> %s", r->filename, md);
            }
        }

        if (result == OK) {
            ap_table_setn (r->headers_out, MD5_HEADER, ap_pstrdup(r->pool, md));
            if (strcasecmp(r->method, M_HEAD) == 0) {
                ap_send_http_header(r);
                ap_rputs("<html>\n", r);
                ap_rputs("<body>\n", r);
                ap_rputs(md, r);
                ap_rputs("\n</body>\n", r);
                ap_rputs("</html>\n", r);
            }
            else if (r->method_number == M_GET) {
                int cmp;

                if (ap_table_get(r->headers_in, MD5_HEADER)) {
                    cmp = (strcmp(ap_table_get(r->headers_in, MD5_HEADER), md) == 0);
                }
                else if (r->args) {
                    cmp = (strcmp(r->args, md) == 0);
                }
                else {
                    cmp = 0;
                }

                if (cmp) {
                    result = HTTP_NOT_MODIFIED;
                }
                else {
                    r->content_type = SVB_CONTENT_TYPE;
                    result = DECLINED;
                }
            }
            else {
                result = HTTP_METHOD_NOT_ALLOWED;
            }
        }
    }

    ap_kill_timeout(r);

    return result;
}

static const handler_rec md5_compare_handlers[] =
{
    {"md5-compare-handler", md5_compare_handler},
    {NULL}
};

module MODULE_VAR_EXPORT md5_compare_module =
{
    STANDARD_MODULE_STUFF,
    module_init,                /* initializer */
    create_dir_config,          /* dir config creater */
    merge_config,               /* dir merger --- default is to override */
    create_server_config,       /* server config */
    merge_config,               /* merge server configs */
    md5_compare_cmds,           /* command table */
    md5_compare_handlers,       /* handlers */
    NULL,                       /* filename translation */
    NULL,                       /* check_user_id */
    NULL,                       /* check auth */
    NULL,                       /* check access */
    NULL,                       /* type_checker */
    NULL,                       /* fixups */
    NULL,                       /* logger */
    NULL,                       /* header parser */
    NULL,                       /* child_init */
    NULL,                       /* child_exit */
    NULL                        /* post read-request */
};

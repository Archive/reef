import pyammonite
import xmlrpclib
#import reef

rpm = None
try:
	import rpm
except:
	print "rpm module not available, trying fallback"

UPDATE_SLICE_LENGTH=100

class SoftcatUpdater:
	def __init__(self):
		# FIXME: should pick up distro from libreef, see bug 7885
		self.distro = 'RedHat70'
		self.cpuArch = 'i386'
		self.server = xmlrpclib.Server(pyammonite.http_url_for_eazel_url_prompt("eazel-services:///services/xmlrpc"))
		self.pkglist = {}
		self.rpmdb = None
		if rpm:
			self.rpmdb = rpm.opendb(0,"/")
		self.load_all_packages()
		
	def load_all_packages(self):
		if rpm:
			db = self.rpmdb
			
			key = db.firstkey()
			h = db[key]
			while (h):
				self.pkglist[h['name']] = h
				key = db.nextkey(key)
				if (key == None):
					h = None
				else:
					h = db[key]

	def get_package_names(self):
		return self.pkglist.keys()

	def get_all_packages(self):
		pkgnames = self.get_package_names()
		packages = []
		i = 0
		while i < len(pkgnames):
			packages.extend(self.server.softcat.getMostRecentPackages(pkgnames[i:i+UPDATE_SLICE_LENGTH],
																	  self.distro))
			i = i + UPDATE_SLICE_LENGTH
		return packages

	def is_update_available(self, product):
		package = product['primaryPackage']
		return self.is_update_available_pkg(package)

	def is_update_available_pkg(self, package):
		if rpm:
			indexes = []
			try:
				indexes = self.rpmdb.findbyname(package['name'])
			except:
				print "Weird package:",package
				return 0

			
			if len(indexes) > 0 and indexes[0] != None:
				header = self.rpmdb[indexes[0]]
				return rpm.labelCompare(("0", package['version'], package['revision']),
										("0", header[rpm.RPMTAG_VERSION], header[rpm.RPMTAG_RELEASE]))
			else:
				return 1 # package not installed
		else:
			return 0

	def get_product_by_package_name(self, name):
		product = self.server.softcat.getProductByPackageName(name,
															  self.distro)
		return product

	def installed_version(self, product):
		package = product['primaryPackage']
		return self.installed_version_pkg(package)

	def installed_version_pkg(self, package):
		if rpm:
			try:
				indexes = self.rpmdb.findbyname(package['name'])
			except:
				print "Weird package:",package
				return 0
			
			if len(indexes) > 0 and indexes[0] != None:
				header = self.rpmdb[indexes[0]]
				return "%s-%s" % (header[rpm.RPMTAG_VERSION], header[rpm.RPMTAG_RELEASE]);

	def available_version(self, product):
		package = product['primaryPackage']
		return "%s-%s" % (package['version'], package['revision'])


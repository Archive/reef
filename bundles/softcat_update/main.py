#!/usr/bin/python

import imp

import reef
import libglade
import string

rpm = None
try:
	import rpm
except:
	print "RPMlib not available"

from gtk import *

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

softcat_updater = reef_import_module("softcat_updater")
updateui = reef_import_module("updateui")

class SoftcatUpdateClient:
	def __init__(self):
		self.updater = softcat_updater.SoftcatUpdater()
		self.updateui = updateui.UpdateUI(self)
		self.current_product = None

	def get_product_by_package_name(self, name):
		return self.updater.get_product_by_package_name(name)

	def is_update_available(self, product):
		return self.updater.is_update_available(product)

	def is_update_available_pkg(self, package):
		return self.updater.is_update_available_pkg(package)

	def get_updated_packages(self):
		return filter(self.is_update_available_pkg, self.updater.get_all_packages())

	def set_current_product(self, product):
		self.current_product = product

	def get_installed_version(self, product):
		return self.updater.installed_version(product)

	def get_installed_version_pkg(self, package):
		return self.updater.installed_version_pkg(package)

	def get_available_version(self, product):
		return self.updater.available_version(product)
	
	def do_install(self):
		reef.open_location_in_this_window("eazel-install:product_id=" +
										  self.current_product['id'])

reef.get_container_widget().add(SoftcatUpdateClient().updateui)

import reef

from gtk import *
from gnome.xmhtml import *

import imp
def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

html_widget = reef_import_module("html_widget")
from html_widget import HTMLWidget

class UpdateUI(GtkVBox):
	def install_button_clicked(self, button, *args):
		self.client.do_install()

	def package_list_select_row(self, clist, row, column, event, *args):
		reef.report_load_underway()
		idle_add(self.show_product, clist, row, column, event, *args)

	def show_product(self, clist, row, column, event, *args):
		package_name = clist.get_row_data(row)
		product = self.client.get_product_by_package_name(package_name)
		installed_version = self.client.get_installed_version(product)
		available_version = self.client.get_available_version(product)
		is_update = self.client.is_update_available(product)
		self.client.set_current_product(product)
		self.product_pane.set_product(product,
									  installed_version,
									  available_version,
									  is_update)
		reef.report_load_complete()

	def show_all_updatable_packages(self):
		self.package_list.freeze()
		self.package_list.clear()
		packages = self.client.get_updated_packages()
		packages.sort(lambda x,y: cmp(x['name'],y['name']))
		for package in packages:
			rownum = self.package_list.append(["",
											   package['name'],
											   self.client.get_installed_version_pkg(package),
											   package['version'] + "-" + package['revision'],
											   package['summary']])
			self.package_list.set_row_data(rownum, package['name'])

		self.package_list.thaw()
		reef.report_load_complete()

	def __init__(self, client):
		self.client = client
		
		GtkVBox.__init__(self)
		self.set_homogeneous(FALSE)
		self.show()

		# Top pane: title label
		label = GtkLabel("Eazel Software Update")
		self.add(label)
		self.set_child_packing(label,FALSE,FALSE,10,PACK_START)
		label.show()

		vpane = GtkVPaned()
		vpane.set_position(250)
		self.add(vpane)
		vpane.show()

		clist = GtkCList(5, ['',
							 "Package",
							 "Installed",
							 "Available",
							 "Summary"])
		self.package_list = clist
		clist.show()

		sw = GtkScrolledWindow()
		sw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
		sw.add(clist)
		
		vpane.add(sw)
		sw.show()

		# Product display pane set to self.product_pane
		prodpane = ProductPane()
		self.product_pane = prodpane
		vpane.add(prodpane)
		prodpane.show()

		# Connect event callbacks
		self.product_pane.install_button.connect("clicked",self.install_button_clicked)
		self.package_list.connect("select-row",self.package_list_select_row)

		reef.report_load_underway()
		idle_add(self.show_all_updatable_packages)

		
 
class ProductPane(GtkVBox):
	def clear_product(self):
		self.product_name_label.set_text("")
		self.installed_version_label.set_text("")
		self.available_version_label.set_text("")
		self.description_box.set("")
		self.install_button.set_sensitive(FALSE)
		self.install_button_label.set_text("Install")
		
	def set_product(self, product, installedversion, availableversion, is_update):
		self.product_name_label.set_text(product['name'])
		self.description_box.set(product['description'])
		if installedversion != None:
			self.installed_version_label.set_text(installedversion)
		else:
			self.installed_version_label.set_text("Not installed")
		self.available_version_label.set_text(availableversion)

		if is_update > 0:
			self.install_button.set_sensitive(TRUE)
			if installedversion != None:
				self.install_button_label.set_text("Update")
			else:
				self.install_button_label.set_text("Install")
		else:
			self.install_button.set_sensitive(FALSE)
	
	def __init__(self):
		GtkVBox.__init__(self)
		self.set_homogeneous(FALSE)
		
		hbox = GtkHBox()
		hbox.set_homogeneous(FALSE)
		self.add(hbox)
		self.set_child_packing(hbox, FALSE, FALSE, 5, PACK_START)
		hbox.show()

		label = GtkLabel()
		self.product_name_label = label
		hbox.add(label)
		label.show()

		button = GtkButton()
		self.install_button = button
		hbox.add(button)
		hbox.set_child_packing(button, TRUE, FALSE, 5, PACK_END)
		button.show()
		button.set_sensitive(FALSE)

		label = GtkLabel("Install")
		self.install_button_label = label
		button.add(label)
		label.show()

		table = GtkTable(5,5,TRUE)
		self.add(table)
		table.show()

		html = HTMLWidget(None)
		self.description_box = html
		html.show()

		table.attach(html, 0, 5, 0, 3)

		label = GtkLabel("Installed Version:")
		table.attach(label, 0, 2, 3, 4)
		label.show()
		label = GtkLabel()
		self.installed_version_label = label
		table.attach(label, 3, 5, 3, 4)
		label.show()

		label = GtkLabel("Available Version:")
		table.attach(label, 0, 2, 4, 5)
		label.show()
		label = GtkLabel()
		self.available_version_label = label
		table.attach(label, 3, 5, 4, 5)
		label.show()

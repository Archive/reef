#!/usr/bin/env python

import sys, os, string, getopt

def header (version, siglength):
	import struct
	return struct.pack ('!BBBBBBBBLL', 0x57, 0x61, 0x6E, 0x64, 0x61, 
		0x20, 0x04 , 0x02, version, siglength)

(options, sourcedirs) = getopt.getopt(sys.argv[1:], 'z:s:n:', ["zipfile=",
															  "signature=",
															  "name="])

sourcedir = None
zipfile = None
zipfiletmp = 0
sigfile = None
destfile = None
name = None

for o, a in options:
	if o in ("-z","--zipfile"):
		zipfile = a
	elif o in ("-s","--signature"):
		sigfile = a
	elif o in ("-n","--name"):
		name = a

if zipfile is None:
	if name is None:
		name = sourcedirs[0] # use the first source directory as the name
else:
	name = zipfile[:-4]

# set destination file
destfile = name+'.svb'

# zip up the dirs if required
if not zipfile:
	tempdir = os.tmpnam()
	zipfile = tempdir+'-make-svb.zip'
	os.system ('mkdir -p %s' % (tempdir))
	for sourcedir in sourcedirs:
		print "sourcedir:",sourcedir
		print "tempdir:",tempdir
		os.system('cp -r %s/* %s' % (sourcedir, tempdir))
	zipfiletmp = 1
	# Clean up SVB dir before zipping
	os.system ('cd %s ; /bin/rm *~' % (tempdir))
	os.system ('cd %s ; /bin/rm *.bak' % (tempdir))

	os.system ('cd %s ; zip -r %s *' % (tempdir, zipfile))
	os.system ('/bin/rm -r %s' % (tempdir))

destfd = open (destfile, 'w')
sig = '' 
if sigfile:
	sig = open (sigfile, 'r').read()

zip = open (zipfile, 'r').read ();

destfd.write (header(0, len (sig)))
destfd.write (sig)
destfd.write (zip)
destfd.close ()

os.system ('/bin/rm -r %s' % (zipfile))

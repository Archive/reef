#!/usr/bin/python

import sys

print "sys.path = '%s'" % `sys.path`
from gtk import *

import reef
import xmlrpclib, os
import async

#from gnome.xmhtml import *
#class HTMLWidget (GtkXmHTML):
#	def __init__ (self, clicked_callback, get_content_callback = None):
#		GtkXmHTML.__init__(self)
#		self.clicked_callback = clicked_callback
#		self.set_dithering(FALSE)  # this forces creation of CC
#		self.set_allow_body_colors(TRUE)
#		self.connect ('activate', self._activate)
#		self.connect ('anchor_track', self._anchor_track)
#
#	def _activate (self, html, info):
#		self.clicked_callback (self, info.href)
#
#	def _anchor_track (self, html, info):
#		if info.href:
#			reef.report_status (info.href)
#		else:
#			reef.report_status ('')
#
#	def set (self, html):
#		self.freeze ()
#		self.source (html)
#		self.thaw ()

from gtkhtml import *
class HTMLWidget (GtkScrolledWindow):
	def __init__(self, clicked_callback, get_content_callback = None):
		GtkScrolledWindow.__init__ (self)
		self.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC)
		self.html = GtkHTML()
		self.html.show ()
		self.add (self.html)
		self.html.load_empty()
		self.clicked_callback = clicked_callback
		self.get_content_callback = get_content_callback
		self.html.connect('url_requested', self._get_content)
		self.html.connect('link_clicked', self._activate)
		self.url = ''

	def set (self, html):
		handle = self.html.begin()
		self.html.write(handle, html)
		self.html.end(handle, HTML_STREAM_OK)

	def _activate (self, html, url):
		if self.clicked_callback:
			self.clicked_callback (self, url)

	def _get_content (self, html, url, handle):
		if not self.get_content_callback:
			self.html.end (handle, HTML_STREAM_ERROR)
			return

		content = self.get_content_callback (self, url)

		if content == None:
			self.html.end (handle, HTML_STREAM_ERROR)
			return

		self.html.write (handle, content)
		self.html.end(handle, HTML_STREAM_OK)
	
class MeerkatClient (GtkHPaned):
	def change_channel (self, clist, r, c, event, *args):
		reef.report_load (0)
		row_data = clist.get_row_data (r)
		html = '<HTML><BODY><H1>%s</H1>' % \
				row_data['title']

		for item in self.mks.meerkat.getItems({'channel': row_data['id']}):
			html = html+'<H3><A HREF="%(link)s">%(title)s</A></H3><P>%(description)s</P>' % item

		self.html.set (html)
		reef.report_load (1)

	def load_external_link (self, widget, href):
		#os.system('gnome-moz-remote --newwin '+href)
		reef.open_location (href, 0)

	def download_listing (self, *args):
		print "download_listing starts...\n"
		self.mks = xmlrpclib.Server("http://www.oreillynet.com/meerkat/xml-rpc/server.php")
		self.listing = self.mks.meerkat.getChannels()
		print "download_listing ends...\n"
		reef.report_load (1)
		idle_add (self.update_listing)

	def update_listing (self, *args):

		self.list.freeze ()
		self.list.clear ()
		for c in self.listing:
			self.list.set_row_data(
				self.list.append ([c['title']]), c)
		self.list.thaw ()
		self.html.set ('')
		self.list.connect ("select_row", self.change_channel)

	def __init__ (self):

		GtkHPaned.__init__ (self)
		self.show ()

		sw = GtkScrolledWindow ()
		sw.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC)
		sw.show ()

		self.list = GtkCList (1, ['Channel'])
		self.list.set_usize(200, 400)
		self.list.show()
		sw.add (self.list);
		self.add (sw);

		vbox = GtkVBox ()
		vbox.show()
		self.add (vbox)

		self.html = HTMLWidget(self.load_external_link)
		self.html.set_usize(400, 400)
		self.html.set('<H1>Downloading channel information</H1>')
		self.html.show()
		vbox.add (self.html)

		#async.apply_async (self.download_listing, [], 
		#			self.update_listing, [])
		#async.reef_async (self.download_listing)
		reef.report_load (0)
		idle_add (self.download_listing)


reef.get_container_widget ().add (MeerkatClient ())

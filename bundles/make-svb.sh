#!/bin/sh

MAGICSTRING="FIMXE - Define a magic string for SVBs"

test -z "$1" && echo $0 '<source directory name>' && exit 1

rm -f $1.zip

cd $1

zip ../$1.zip *

cd ..

gpg -a --sign $1.zip

(echo $MAGICSTRING;cat $1.zip.asc $1.zip) > $1.svb



import reef

from gtk import *
from gnome.xmhtml import *

import libglade
import imp
def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

html_widget = reef_import_module("html_widget")
from html_widget import HTMLWidget

class NewssourceUI:
	def go_button_clicked(self, button, *args):
		news_source = self.source_menu.get_active().get_data("value")
		reef.report_load_underway()
		idle_add(self.client.get_news, news_source, self.set_result)

	def set_result(self, result_text):
		self.html_widget.set(result_text)
		reef.report_load_complete()

	def __init__(self, client, dict):
		self.client = client
		
		xml = libglade.GladeXML(reef.get_bundle_base() + "/newssource.glade", "vbox1")
		self.button = xml.get_widget("button1")
		vbox = xml.get_widget("vbox1")
		self.widget = vbox
		optionmenu = xml.get_widget("optionmenu1")
		menu = GtkMenu()
		self.source_menu = menu
		for key in dict.keys():
			item = GtkMenuItem(key)
			item.set_data("value",dict[key])
			item.show()
			menu.append(item)
		menu.set_active(0)
		menu.show()
		optionmenu.set_menu(menu)
		
		html = HTMLWidget(self.client.load_external_link)
		vbox.add(html)
		vbox.set_child_packing(html,TRUE,TRUE,10,PACK_END)
		html.show()
		self.html_widget = html

		# Connect event callbacks
		self.button.connect("clicked",self.go_button_clicked)
		

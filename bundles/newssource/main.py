#!/usr/bin/python

import imp
from gtk import *
import soaplib
import reef

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

newssourceui = reef_import_module("newssourceui")
newsdict = {"Yahoo": "yahoo",
			"Reuters": "reuters",
			"7AM": "7am",
			"NewsLinx": "newslinx"}

class NewssourceClient:
	def __init__(self):
		self.ui = newssourceui.NewssourceUI(self, newsdict)
		self.server = soaplib.ServerProxy("http://www.SoapClient.com/xml/SQLDataSoap.WSDL")

	def get_news(self, news_source, set_text):
		set_text(self.server.ProcessSRL(SRLFile="NEWS.SRI",
										RequestName=news_source))

	def load_external_link (self, widget, href):
		#os.system('gnome-moz-remote --newwin '+href)
		reef.open_location_prefer_existing_window (href)
		
reef.get_container_widget().add(NewssourceClient().ui.widget)

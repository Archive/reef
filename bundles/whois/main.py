#!/usr/bin/python

import imp
from gtk import *
import soaplib
import reef

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

whoisui = reef_import_module("whoisui")

class WhoisClient:
	def __init__(self):
		self.ui = whoisui.WhoisUI(self)
		self.server = soaplib.ServerProxy("http://www.SoapClient.com/xml/SQLDataSoap.WSDL")

	def do_whois(self, whois_domain, set_text):
		set_text(self.server.ProcessSRL(SRLFile="WHOIS.SRI",
										RequestName="whois",
										key=whois_domain))


reef.get_container_widget().add(WhoisClient().ui)

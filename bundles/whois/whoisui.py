import reef

from gtk import *
from gnome.xmhtml import *

import imp
def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

html_widget = reef_import_module("html_widget")
from html_widget import HTMLWidget

class WhoisUI(GtkVBox):
	def go_button_clicked(self, button, *args):
		whois_domain = self.entry.get_text()
		self.client.do_whois(whois_domain, self.set_result)

	def set_result(self, result_text):
		self.html_widget.set(result_text)

	def __init__(self, client):
		self.client = client
		
		GtkVBox.__init__(self)
		self.set_homogeneous(FALSE)
		self.show()

		# Top pane: title label
		label = GtkLabel("Whois")
		self.add(label)
		self.set_child_packing(label,FALSE,FALSE,10,PACK_START)
		label.show()

		hbox = GtkHBox()
		hbox.set_homogeneous(FALSE)
		self.add(hbox)
		self.set_child_packing(hbox,FALSE,FALSE,10,PACK_START)
		hbox.show()

		entry = GtkEntry()
		hbox.add(entry)
		entry.show()
		self.entry = entry

		button = GtkButton("Whois")
		hbox.add(button)
		button.show()
		self.button = button

		html = HTMLWidget(None)
		self.add(html)
		html.show()
		self.html_widget = html

		# Connect event callbacks
		self.button.connect("clicked",self.go_button_clicked)
		

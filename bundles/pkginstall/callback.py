#
# install callback class
#

import gtk, imp, reef

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname, file, pathname, description)
	file.close()
	return ret

corba_util = reef_import_module('corba_util')
corba_util.load_idl_paths()
PackageData = reef_import_module('PackageData')

import CORBA
import GNOME__POA.Trilobite

class InstallCallback(GNOME__POA.Trilobite.InstallCallback):

    def __init__(self, orb, html, html_path):
        self.orb = orb
        self.html = html
        self.html_path = html_path
        self.widget = {}
        reef.report_load(0)	# report_load_underway
        self.log_text = ''
        self.status_text = ''
        
    def __del__(self):
        pass

    def refresh_all(self):
        "called when we may have changed the html, so we might have all new widgets"
        if self.widget.has_key('status'):
            self.widget['status'].set_text(self.status_text)
        if self.widget.has_key('log'):
            self.widget['log'].set_text(self.log_text)
        self.html.queue_resize()

    def status_set(self, text):
        if not self.widget.has_key('status'):
            return
        label = self.widget['status']
        label.set_text(text)
        self.status_text = text
        self.html.queue_resize()
        
    def log_append(self, text):
        if len(self.log_text) > 0:
            self.log_text = self.log_text + '\n'
        self.log_text = self.log_text + text
        self.log_update()

    def log_update(self):
        if not self.widget.has_key('log'):
            return
        label = self.widget['log']
        label.set_text(self.log_text)
        # lame way to trigger a redraw of the embedded widget. :(
        # i tried exposing the gtk_html_embedded_changed() call, but it's not sufficient -- even if
        # run from a timer.  the html widget doesn't figure out that it needs to redraw unless the
        # mouse cursor leaves its xwindow, or unless i explicitly queue a redraw on it (like below).
        ## container = label.get_ancestor(gtkhtml.GtkHTMLEmbedded.get_type())
        ## gtk.timeout_add(1000, foo, container)
        self.html.queue_resize()
    
    def dependency_check(self, package, needs):
        self.log_append('Dependency: ' + str(PackageData.PackageData(package)) + ' needs ' +
                        str(PackageData.PackageData(needs)))
        self.status_set('Checking dependencies ...')
        
    def file_uniqueness_check (self, package):
        self.log_append('Uniqueness checking ' + str(PackageData.PackageData(package)))
        self.status_set('Checking package fit ...')

    def file_conflict_check (self, package):
        self.log_append('File conflict checking ' + str(PackageData.PackageData(package)))
        self.status_set('Checking package contents ...')

    def feature_consistency_check (self, package):
        self.log_append('Consistency checking ' + str(PackageData.PackageData(package)))
        self.status_set('Checking package consistency ...')

    def preflight_check (self, op, package_tree, total_size, total_num):
        total_filesize = 0
        reef.report_load(1)	# report_load_complete
        for pack in package_tree:
            total_filesize = total_filesize + pack.filesize
        msg = 'About to ' + PackageData.op_name(op) + ' ' + str(total_num) + ' packages (' + \
              PackageData.byte_string(total_filesize) + ')'
        packlist = PackageData.inflate_tree(package_tree)
        flatlist = PackageData.flatten(packlist)
#        for pack in flatlist:
#            print '    ' + str(pack)
#        print
#        answer = raw_input('Proceed? ')
#        if answer != None and answer != '' and (answer[0] == 'y' or answer[0] == 'Y'):
#            return 1
        return 0
    
    def install_failed(self, packlist):
        reef.report_load(1)	# report_load_complete
        tree = PackageData.inflate_tree(packlist)
        for x in tree:
            list = x.scan_for_errors()
            for s in list:
                print "Install failed:", s
        
    def done(self, result):
        if not result:
            print 'Abject failure.'
            self.log_append('Install failed :(')
            self.status_set('Install failed or aborted or something.')
        else:
            print 'Success!'
            self.log_append('Install was successful!  Party time!')
            self.status_set('Install succeeded!')
        # Terminate main loop and return 0 to say "thanks, but no thanks..."
        self.orb.shutdown(1)

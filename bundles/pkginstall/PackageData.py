#!/usr/bin/python
#
# implements a PackageData type descended from the CORBA PackageDataStruct.
# parts of this are based heavily on the C PackageData code.
#
# (C) 2001 Eazel, Inc.
# GPL license, etc.
# Authors: Robey Pointer

import string
import CORBA
import GNOME.Trilobite.Eazel

class Distribution(GNOME.Trilobite.Eazel.DistributionStruct):
    def __init__(self, olddist=None):
        if olddist == None:
            self.name = 'unknown'
            self.major = 0
            self.minor = 0
            return
        self.name, self.major, self.minor = olddist.name, olddist.major, olddist.minor

    def __str__(self):
        return "%s %d.%d" % (self.name, self.major, self.minor)

class PackageDependency(GNOME.Trilobite.Eazel.PackageDependencyStruct):
    def __init__(self, olddep=None):
        if olddep == None:
            self.sense = ''
            self.version = ''
            self.package = None
            return
        self.sense, self.version = olddep.sense, olddep.version
        self.package = None

class PackageBreaks(GNOME.Trilobite.Eazel.PackageBreaksStruct):
    def __init__(self, oldbrk=None):
        if oldbrk == None:
            self.package = None
            self._type = GNOME.Trilobite.Eazel.PACKAGE_FILE_CONFLICT
            self.files = []
            self.features = []
            return
        self.package = None
        self._type = oldbrk.u._d
        if self._type == GNOME.Trilobite.Eazel.PACKAGE_FILE_CONFLICT:
            self.files = oldbrk.u.files
        elif self._type == GNOME.Trilobite.Eazel.PACKAGE_FEATURE_MISSING:
            self.features = oldbrk.u.features
            
class PackageData(GNOME.Trilobite.Eazel.PackageDataStruct):
    def __init__(self, oldpack=None):
        if oldpack == None:
            self.name = ''
            self.version = ''
            self.filename, self.eazel_id, self.suite_id, self.archtype, self.release = '', '', '', '', ''
            self.summary, self.description, self.install_root, self.md5 = '', '', '', ''
            self.distribution = Distribution()
            self.bytesize, self.filesize = 0, 0
            self.toplevel, self.anchor = 0, 0
            self.status = GNOME.Trilobite.Eazel.UNKNOWN_STATUS
            self.modify_status = GNOME.Trilobite.Eazel.UNTOUCHED
            self.modifies = []
            self.provides = []
            self.features = []
            self.depends = []
            self.breaks = []
            return
        # we were passed in a PackageDataStruct; copy it over
        self.name, self.version = oldpack.name, oldpack.version
        self.distribution = Distribution(oldpack.distribution)
        self.filename, self.eazel_id, self.suite_id = oldpack.filename, oldpack.eazel_id, oldpack.suite_id
        self.archtype, self.release, self.summary = oldpack.archtype, oldpack.release, oldpack.summary
        self.description, self.install_root, self.md5 = oldpack.description, oldpack.install_root, oldpack.md5
        self.bytesize, self.filesize, self.toplevel = oldpack.bytesize, oldpack.filesize, oldpack.toplevel
        self.anchor, self.status, self.modify_status = oldpack.anchor, oldpack.status, oldpack.modify_status
        self.provides, self.features = oldpack.provides, oldpack.features
        self.modifies = []
        self.depends = []
        self.breaks = []
        
    def get_readable_name(self):
        "returns a human-readable name for the package (usually the package name and version)"
        if self.name != '' and self.version != '':
            n = string.find(self.version, '.200')
            if n < 0 or n + 5 >= len(self.version):
                return "%s v%s" % (self.name, self.version)
            # dated hourly build:  '200104301225'
            n = n + 5
            datenum = self.version[n:n+8]
            mon, day, hr, min = int(datenum[0:2]), int(datenum[2:4]), int(datenum[4:6]), int(datenum[6:8])
            timestr = time.strftime("%d %b, %H:%M", (1990, mon, day, hr, min, 0, 0, 0, 0))
            return "%s of %s" % (self.name, timestr)
        if self.name != '':
            return self.name
        if self.eazel_id != '':
            return "Softcat package #%s" % self.eazel_id
        if self.suite_id != '':
            return "Softcat suite #%s" % self.suite_id
        if len(self.features) > 0:
            return "file %s" % self.features[0]
        return "unknown (blank) package"

    def __str__(self):
        return self.get_readable_name()
    
    def get_error_string(self, path=None):
        """
        returns a string describing an error that occurred in this package
        (or an empty string if no error occurred here).
        path is a list of PackageData's: top of tree at 0, immediate parent at the end
        """
        
        if path != None and len(path) > 0:
            top = path[0]
            parent = path[len(path)-1]
            if top == parent:
                parent = None
        else:
            top = parent = None

        msg = ""
            
        if self.status == GNOME.Trilobite.Eazel.UNKNOWN_STATUS:
            msg = "(unknown)"
        if self.status == GNOME.Trilobite.Eazel.CANCELLED:
            if top != None and len(self.modifies) > 0:
                msg = "%s was cancelled" % self.get_readable_name()
        if self.status == GNOME.Trilobite.Eazel.SOURCE_NOT_SUPPORTED:
            msg = "%s is a source packages, which is not supported" % self.get_readable_name()
        if self.status == GNOME.Trilobite.Eazel.DEPENDENCY_FAIL:
            if len(self.depends) > 0:
                if parent != None and parent.status == GNOME.Trilobite.Eazel.BREAKS_DEPENDENCY:
                    if top == None:
                        msg = "%s would break other packages" % self.get_readable_name()
                else:
                    msg = "%s would break" % self.get_readable_name()
        # FILE_CONFLICT handled elsewhere
        if self.status == GNOME.Trilobite.Eazel.BREAKS_DEPENDENCY:
            if top != None:
                msg = "%s would break %s" % (self.get_readable_name(), top.get_readable_name())
            else:
                msg = "%s would break other packages" % str(self)
        if self.status == GNOME.Trilobite.Eazel.INVALID:
            msg = "%s is damaged" % str(self)
        if self.status == GNOME.Trilobite.Eazel.CANNOT_OPEN:
            if parent != None:
                msg = "%s requires %s, which could not be found on the server" % (str(top), str(self))
            else:
                msg = "%s for %s could not be found on the server" % (str(self), str(self.distribution))
        # PARTLY_RESOLVED has nothing
        # RESOLVED has nothing
        if self.status == GNOME.Trilobite.Eazel.ALREADY_INSTALLED:
            if len(self.modifies) == 0:
                msg = "%s is already installed" % str(self)
        if self.status == GNOME.Trilobite.Eazel.CIRCULAR_DEPENDENCY:
            if parent == None:
                msg = "%s depends on itself (internal error)" % str(self)
            else:
                if parent.status == GNOME.Trilobite.Eazel.CIRCULAR_DEPENDENCY:
                    if top != None and str(self) == str(top):
                        msg = "%s depends on itself (internal error)" % str(self)
                    else:
                        if len(path) >= 3:
                            cause = path[len(path)-2]
                            # FIXME: i doubt users know or care what a 'mutex' is
                            msg = "%s and %s are mutexed because of %s" % (str(self), str(top), str(cause))
                        else:
                            msg = "%s and %s exclude each other, but are both needed" % (str(self), str(top))
        if self.status == GNOME.Trilobite.Eazel.PACKSYS_FAILURE:
            msg = "Cannot access the local package system"

        # if we don't have an error so far, check modify status
        if msg == '' and self.status == GNOME.Trilobite.Eazel.CANCELLED:
            if self.modify_status == GNOME.Trilobite.Eazel.DOWNGRADED:
                msg = "%s, which is newer, needs to be downgraded (and downgrading is not enabled)" % str(self)
            if self.modify_status == GNOME.Trilobite.Eazel.UPGRADED:
                msg = "%s, which is older, needs to be upgraded (and upgrading is not enabled)" % str(self)

        return msg

    def scan_for_errors(self, list=[], path=[]):
        "returns list of error messages, found by scanning this package and its children"
        msg = self.get_error_string(path)
        if msg != '':
            list.append(msg)
        path.append(self)
        for p in self.depends:
            p.package.scan_for_errors(list, path)
        for p in self.modifies:
            p.scan_for_errors(list, path)
        for p in self.breaks:
            p.package.scan_for_errors(list, path)
            pass
        path.pop()
        return list

    def dump(self, deep=0, show_provides=0, indent=0):
        out = ''
        name = self.name
        if name == '':
            name = '(no name)'
        minor = self.release
        if minor != '':
            minor = '-' + minor
        out = out + (' ' * indent)
        out = out + 'Package %s v%s%s (arch %s) for %s\n' % \
              (name, self.version, minor, self.archtype, str(self.distribution))

        indent = indent + 4
        out = out + (' ' * indent)
        out = out + status_string(self.status) + '/' + modify_status_string(self.modify_status)

        if self.eazel_id != '':
            out = out + ', EID ' + self.eazel_id
        if self.bytesize > 0:
            out = out + ', ' + byte_string(self.bytesize) + ' installed'
        if self.filesize > 0:
            out = out + ', ' + byte_string(self.filesize) + ' file'
        if self.toplevel:
            out = out + ', TOPLEVEL'
        out = out + '\n'

        if self.filename != '':
            out = out + (' ' * indent) + 'Filename: ' + self.filename + '\n'
        if self.md5 != '':
            out = out + (' ' * indent) + 'MD5: ' + self.md5 + '\n'
        if self.install_root != '':
            out = out + (' ' * indent) + 'Install root: ' + self.install_root + '\n'
        if self.summary != '':
            out = out + (' ' * indent) + 'Summary: ' + self.summary + '\n'
        if self.description != '':
            out = out + (' ' * indent) + 'Description:\n' + self.description + '\n'

        if len(self.features) > 0:
            out = out + (' ' * indent) + 'Features: '
            cont = ''
            for item in self.features:
                out = out + cont + item
                cont = '; '
            out = out + '\n'

        if show_provides and len(self.provides) > 0:
            out = out + (' ' * indent) + 'Provides:\n'
            indent = indent + 4
            for item in self.provides:
                out = out + (' ' * indent) + item + '\n'
            indent = indent - 4

        if len(self.depends) > 0:
            out = out + (' ' * indent) + 'Depends: '
            out = out + self.dump_deplist(deep, show_provides, indent)
            out = out + '\n'

        if len(self.modifies) > 0:
            out = out + (' ' * indent) + 'Modifies: '
            out = out + self.dump_modlist(deep, show_provides, indent)
            out = out + '\n'

        if len(self.breaks) > 0:
            out = out + (' ' * indent) + 'Breaks: '
            out = out + self.dump_brklist(deep, show_provides, indent)
            out = out + '\n'
            
        return out

    def dump_deplist(self, deep=0, show_provides=0, indent=0):
        out = ''
        if deep:
            out = out + '\n'
        cont = ''
        for dep in self.depends:
            if deep:
                out = out + dep.package.dump(deep, show_provides, indent+4)
                if dep.version != '':
                    out = out + (' ' * (indent+8)) + 'Solves ' + dep.package.name + ' ' + \
                          dep.sense + ' ' + dep.version + '\n'
            else:
                out = out + cont
                if dep.version != '':
                    out = out + '[need: ' + dep.sense + ' ' + dep.version + '] '
                out = out + str(dep.package)
                cont = '; '
        return out

    def dump_modlist(self, deep=0, show_provides=0, indent=0):
        out = ''
        if deep:
            out = out + '\n'
        cont = ''
        for pack in self.modifies:
            if deep:
                out = out + pack.dump(deep, show_provides, indent+4)
            else:
                out = out + cont + str(pack)
                cont = ', '
        return out

    def dump_brklist(self, deep=0, show_provides=0, indent=0):
        out = ''
        if deep:
            out = out + '\n'
        cont = ''
        for brk in self.breaks:
            if brk._type == GNOME.Trilobite.Eazel.PACKAGE_FILE_CONFLICT:
                name = 'file conflict'
                list = brk.files
            elif brk._type == GNOME.Trilobite.Eazel.PACKAGE_FEATURE_MISSING:
                name = 'feature missing'
                list = brk.features
            else:
                name = '???'
                list = []
            if deep:
                out = out + (' ' * (indent+4)) + '[' + name + ': ' + dump_strlist(list) + ']\n'
                out = out + brk.package.dump(deep, show_provides, indent+4)
            else:
                out = out + cont + '[' + name + ': ' + dump_strlist(list) + '] ' + str(brk.package)
                cont = '; '
        return out

def dump_strlist(list):
    out, cont = '', ''
    for item in list:
        out = out + cont + item
        cont = ', '
    return out

def status_string(status):
    if status == GNOME.Trilobite.Eazel.UNKNOWN_STATUS:
        return 'UNKNOWN_STATUS'
    if status == GNOME.Trilobite.Eazel.SOURCE_NOT_SUPPORTED:
        return 'SOURCE_NOT_SUPPORTED'
    if status == GNOME.Trilobite.Eazel.DEPENDENCY_FAIL:
        return 'DEPENDENCY_FAIL'
    if status == GNOME.Trilobite.Eazel.FILE_CONFLICT:
        return 'FILE_CONFLICT'
    if status == GNOME.Trilobite.Eazel.BREAKS_DEPENDENCY:
        return 'BREAKS_DEPENDENCY'
    if status == GNOME.Trilobite.Eazel.INVALID:
        return 'INVALID'
    if status == GNOME.Trilobite.Eazel.CANNOT_OPEN:
        return 'CANNOT_OPEN'
    if status == GNOME.Trilobite.Eazel.PARTLY_RESOLVED:
        return 'PARTLY_RESOLVED'
    if status == GNOME.Trilobite.Eazel.RESOLVED:
        return 'RESOLVED'
    if status == GNOME.Trilobite.Eazel.CANCELLED:
        return 'CANCELLED'
    if status == GNOME.Trilobite.Eazel.ALREADY_INSTALLED:
        return 'ALREADY_INSTALLED'
    if status == GNOME.Trilobite.Eazel.CIRCULAR_DEPENDENCY:
        return 'CIRCULAR_DEPENDENCY'
    if status == GNOME.Trilobite.Eazel.PACKSYS_FAILURE:
        return 'PACKSYS_FAILURE'
    return '???'

def modify_status_string(status):
    if status == GNOME.Trilobite.Eazel.UPGRADED:
        return 'UPGRADED'
    if status == GNOME.Trilobite.Eazel.DOWNGRADED:
        return 'DOWNGRADED'
    if status == GNOME.Trilobite.Eazel.INSTALLED:
        return 'INSTALLED'
    if status == GNOME.Trilobite.Eazel.UNINSTALLED:
        return 'UNINSTALLED'
    return 'UNTOUCHED'

def inflate_tree(packlist):
    """
    given a CORBA list of PackageDataStruct's, re-inflate them into a directed graph.
    returns the list of toplevel packages.
    """

    outlist = []
    md5table = {}
    for p in packlist:
        assert p.md5 != ''
        pd = PackageData(p)
        md5table[p.md5] = pd
        if p.anchor:
            outlist.append(pd)
    # now set up links
    for p in packlist:
        pd = md5table[p.md5]
        for dep in p.depends:
            pdep = PackageDependency(dep)
            pdep.package = md5table[dep.package_md5]
            pd.depends.append(pdep)
        for brk in p.breaks:
            pbrk = PackageBreaks(brk)
            pbrk.package = md5table[brk.package_md5]
            pd.breaks.append(pbrk)
        for mod in p.modifies:
            pdep.modifies.append(md5table[mod])
    return outlist

def flatten_int(packtree, outlist):
    "given a PackageData tree, return a flat list of the unique packages in it"
    for pack in packtree:
        if not pack in outlist:
            outlist.append(pack)
        for dep in pack.depends:
            flatten_int([dep.package], outlist)
        for brk in pack.breaks:
            flatten_int([brk.package], outlist)
        flatten_int(pack.modifies, outlist)
    return outlist

def flatten(packtree):
    outlist = []
    return flatten_int(packtree, outlist)
    
def byte_string(bytes, suffix='bytes'):
    "convert a number of bytes into a compact (MB, KB, or bytes) string representation"
    if bytes < 0x4000L:		# 16KB
        return str(bytes) + ' ' + suffix
    if bytes < 0x200000L:	# 2MB
        return str(round(bytes/102.4)/10.0) + ' KB'
    if bytes < 0x80000000L:	# 2GB
        return str(round(bytes/(1024.0*102.4))/10.0) + ' MB'
    return str(round(bytes/(1024.0*1024.0*102.4))/10.0) + ' GB'

def op_name(op):
    if op == GNOME.Trilobite.Eazel.OPERATION_INSTALL:
        return "install"
    if op == GNOME.Trilobite.Eazel.OPERATION_UNINSTALL:
        return "uninstall"
    if op == GNOME.Trilobite.Eazel.OPERATION_REVERT:
        return "revert"

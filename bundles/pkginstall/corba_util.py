#
# handle the weird corba stuff
#

IDLPATH = [ '/usr/idl', '/usr/share/idl', '/gnome/idl', '/gnome/share/idl' ]

import os, string


def load_idl_paths():
    paths = []
    if os.environ.has_key('IDLPATH'):
        paths = string.split(os.environ['IDLPATH'], ':')
    for folder in IDLPATH:
        if not folder in paths:
            paths.append(folder)
    os.environ['IDLPATH'] = string.join(paths, ':')

def load_idl(CORBA, filename):
    for folder in string.split(os.environ['IDLPATH'], ':'):
        path = folder + '/' + filename
        if os.access(path, os.R_OK):
            CORBA._load_idl(path)
            return
    raise IOError, 'No IDL in path.'

def oaf_activate_old(orb, id):
    # oaf python bindings would be nice.
    try:
        infile = os.popen('oaf-client -s "repo_ids.has(\'' + id + '\')"', 'r', 8192)
    except:
        return None

    # Read IOR
    ior = infile.readline(8192)
    while not ior[0:4] == "IOR:" and len(ior) > 0:
        ior = infile.readline (8192)
    if len(ior) == 0:
        raise IOError, "Can't find install service."

    # Get the  object
    server = orb.string_to_object(ior)
    if server is None: 
        raise IOError, 'Nil object reference!'
    return server

#
# "install" bundle
#

import gtk, gtkhtml, gnome.ui, sys, os, os.path, string, imp, reef

def reef_import_module(modname):
    [file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
    ret = imp.load_module(modname, file, pathname, description)
    file.close()
    return ret

HTML_PATH = os.path.join(reef.get_bundle_base(), 'html')

BAD_no_corba = """
You don't have orbit-python installed.
This module is required to make the install SVB work.
I must sadly abort now.
"""

BAD_no_bonobo = """
You don't have bonobo-python installed.
This one is tricky, because you need the CVS version, with
Robey's extra patches (or it'll core on you).
I must sadly abort now.
"""

BAD_gtkhtml = """
You have an old version of gnome-python installed.
This module requires Robey's patch to implement GtkHTMLEmbedded.
I must sadly abort now.
"""

BAD_no_trilobite = """
You don't have Trilobite installed.
(Or: You don't have a recent build.)
I need access to the Trilobite IDL files.
I must sadly abort now.
"""

def bail(reason):
    boo = gnome.ui.GnomeErrorDialog(reason)
    boo.run_and_close()
    reef.report_load(1)		# report_load_complete
    # hack to cause the script to end.
    raise 'bail.'

corba_util = reef_import_module('corba_util')

corba_util.load_idl_paths()

try:
    import bonobo, oaf
except:
    bail(BAD_no_bonobo)
    
try:
    import CORBA
except:
    bail(BAD_no_corba)

try:
    import GNOME.Trilobite
    import GNOME__POA.Trilobite
except:
    bail(BAD_no_trilobite)

try:
    corba_util.load_idl(CORBA, 'Bonobo_Unknown.idl')
except:
    bail("For some reason I can't find Bonobo_Unknown.idl")

# TEMPORARY fix until gnome-python incorporates robey's GtkHTMLEmbedded fixes
try:
    x = gtkhtml.GtkHTMLEmbedded()
except:
    bail(BAD_gtkhtml)


#orb = CORBA.ORB_init ([], CORBA.ORB_ID)
if not oaf.is_initialized():
    orb = oaf.init()
else:
    orb = oaf.orb_get()
poa = orb.resolve_initial_references("RootPOA")
bonobo.init(orb)

PackageData = reef_import_module('PackageData')
callback = reef_import_module('callback')

def go():
    global cb, installer
    
    try:
        #    server = corba_util.oaf_activate(orb, 'IDL:GNOME/Trilobite/Install:1.0')
        corba_server = oaf.activate("repo_ids.has('IDL:GNOME/Trilobite/Install:1.0')", [])
    except:
        bail("For some reason, I'm unable to activate the Install object.")

    try:
        bonobo_server = bonobo.client_from_corba(corba_server)
    except:
        corba_server.unref()
        bail("For some reason, I'm unable to activate the Install object.")
        
    # Now get the actual Trilobite/Eazel/Install object
    installer = bonobo_server.query_interface('IDL:GNOME/Trilobite/Install:1.0')
    assert installer != None
    print 'FOO!'
    corba_server.unref()

    pack = PackageData.PackageData()
    pack.name = 'xfig'
    cat = GNOME.Trilobite.Eazel.CategoryStruct()
    cat.name = 'foo'
    cat.packages = [ pack ]

    # heeeeere we go!
    installer.install_packages ([cat], '', poa.servant_to_reference(cb))
    return 0




def make_html(filename):
    try:
        f = open(HTML_PATH + '/' + filename, 'r')
    except IOError:
        return '<b>(Error loading ' + filename + ')</b>'

    out = f.read()
    f.close()
    return out

def do_html_show_log(button, html):
    cb.widget = {}
    html.load_from_string(make_html('start.html'))
    cb.refresh_all()
#    html.queue_resize()

def do_html_hide_log(button, html):
    print '--> hide log!'
    cb.widget = {}
    html.load_from_string(make_html('no-log.html'))
    cb.refresh_all()
#    html.queue_resize()
    
def attach_widget(embedded, widget):
    widget.show()
    embedded.add(widget)
    widget._parent = embedded

def object_broker(html, object, cb):
    print 'make object: ' + str(object.name)
    if object.name != None and cb.widget.has_key(object.name):
        # previously existing widget
        # CRASH HORRIBLY HERE.  i bet remove() unref's the widget into oblivion.
        # py-gtk doesn't define ref/unref on GtkObject, sadly.
        cb.widget[object.name].ref()
        print 'ref!'
        cb.widget[object.name]._parent.remove(cb.widget[object.name])
        attach_widget(object, cb.widget[object.name])
        cb.widget[object.name].unref()
        return 1
    if object.type == 'label':
        if object.data == None:
            object.data = ''
        label = gtk.GtkLabel(object.data)
        label.set_justify(gtk.JUSTIFY_LEFT)
        attach_widget(object, label)
        cb.widget[object.name] = label
        return 1
    if object.type == 'progress-bar':
        bar = gtk.GtkProgressBar()
        attach_widget(object, bar)
        cb.widget[object.name] = bar
        return 1
    if object.type == 'html':
        # embed another gtkhtml inside this one!  *insane giggle*
        subscroll = gtk.GtkScrolledWindow()
        subscroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        subhtml = gtkhtml.GtkHTML()
        subscroll.add(subhtml)
        subscroll.show()
        subhtml.connect('object_requested', object_broker, cb)
        subhtml.load_from_string(object.data)
        subhtml.show()
        attach_widget(object, subscroll)
        cb.widget[object.name] = subhtml
        return 1
    if object.type == 'button':
        button = gtk.GtkButton()
        label = gtk.GtkLabel(object.data)
        label.show()
        button.add(label)
        attach_widget(object, button)
        cb.widget[object.name] = button
        if object.name == 'show-log':
            button.connect('clicked', do_html_show_log, html)
        elif object.name == 'hide-log':
            button.connect('clicked', do_html_hide_log, html)
        return 1
    return 0

class Canary:
    "notice when we're being destroyed."
    def __init__(self):
        pass
    def __del__(self):
        global installer
        sys.stderr.write('CANARY!\n')
        installer.unref()

c = Canary()


scroll = gtk.GtkScrolledWindow()
scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

html = gtkhtml.GtkHTML()
cb = callback.InstallCallback(orb, html, HTML_PATH)
poa.activate_object(cb)
html.show()
html.connect('object_requested', object_broker, cb)
html.load_from_string(make_html('start.html'))
scroll.add(html)
scroll.show()

reef.get_container_widget().add(scroll)
reef.set_title ('INSTALL SOMETHING PLEASE')

#poa._get_the_POAManager().activate()
#orb.run()
gtk.timeout_add(1000, go)
#bonobo.main()

#installer.delete_files()

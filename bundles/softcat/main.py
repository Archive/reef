#!/usr/bin/python

import imp

import reef
import libglade
import string

rpm = None
try:
	import rpm
except:
	print "RPMlib not available"

from gtk import *

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

softcat = reef_import_module("softcat")
softcatui = reef_import_module("softcatui")

class SoftcatClient:
	# methods delegated to softcat server object
	def get_categories(self):
		return self.softcat.get_categories()

	def get_subcategories(self, category):
		return self.softcat.get_subcategories(category)

	def get_products(self, subcategory):
		num_products = self.softcat.get_product_count(subcategory)
		return self.softcat.get_products(subcategory, 1, num_products)

	def get_product(self, product_id):
		return self.softcat.get_product(product_id)

	def is_subcategory(self, subcategory):
		return self.softcat.is_subcategory(subcategory)

	def get_product_count(self, category):
		return self.softcat.get_product_count(category)

	def get_package(self, package_name):
		return self.softcat.get_package(package_name)

	# my own methods
	def get_installed_version(self, product):
		if rpm:
			indexes = self.rpmdb.findbyname(product['primaryPackageName'])
			if len(indexes) > 0:
				header = self.rpmdb[indexes[0]]
				return "%s-%s" % (header[rpm.RPMTAG_VERSION],
								  header[rpm.RPMTAG_RELEASE])
			else:
				return None

	def get_available_version(self, product):
		package = product['primaryPackage']
		return "%s-%s" % (package['version'], package['revision'])
		
	def set_current_product(self, product):
		self.current_product = product

	def is_installed(self, product):
		if rpm:
			package = product['primaryPackage']
			indexes = self.rpmdb.findbyname(package['name'])
			return (len(indexes) > 0)

	def is_update_available(self, product):
		if rpm:
			package = product['primaryPackage']
			indexes = self.rpmdb.findbyname(package['name'])
			if len(indexes) > 0:
				header = self.rpmdb[indexes[0]]
				return rpm.labelCompare(("0", package['version'], package['revision']),
										("0", header[rpm.RPMTAG_VERSION], header[rpm.RPMTAG_RELEASE]))
			else:
				return 1 # package not installed
		else:
			return 0

	def do_install(self):
		reef.open_location_in_this_window("eazel-install:product_id=" +
										  self.current_product['id'])

	def __init__ (self):
		self.softcat = softcat.Softcat()
		self.softcatui = softcatui.SoftcatUI(self)
		self.current_product = None
		self.rpmdb = None
		if rpm:
			self.rpmdb = rpm.opendb()

reef.get_container_widget().add(SoftcatClient().softcatui)

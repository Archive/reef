import pyammonite
import xmlrpclib
import reef

rpm = None
try:
	import rpm
except:
	print "rpm module not available, trying fallback"

class Softcat:
	def __init__(self):
		# FIXME: should pick up distro from libreef, see bug 7885
		self.distro = 'RedHat70'
		self.cpuArch = 'i386'
		self.server = xmlrpclib.Server(pyammonite.http_url_for_eazel_url_prompt("eazel-services:///services/xmlrpc"))
		self.searchTypes = {"SEARCH_BY_NAME": 1,
				    "SEARCH_BY_DESCRIPTION": 2,
				    "SEARCH_BY_FULLTEXT": 3}
		
	def get_categories(self):
		return self.server.softcat.getTopLevelCategories()
	
	def get_subcategories(self, category):
		return self.server.softcat.getSubcategories(category['id'])
	
	def get_products(self, subcategory, startIndex, pageLength):
		return self.server.softcat.getProducts(subcategory['parentID'],
												   subcategory['id'],
												   self.distro,
												   startIndex,
												   pageLength)

	def find_products(self, param, searchType, startIndex, pageLength):
		return self.server.softcat.findProducts(param, searchType,
							self.distro, startIndex,
							pageLength)

	def get_product(self, productID):
		return self.server.softcat.getProduct(productID, self.distro)

	def load_product(self, product):
		return self.get_product(product['id'])

	def is_subcategory(self, subcategory):
			return (subcategory.has_key('parentID') and
				subcategory['parentID'] != None)

	def get_product_count(self, category):
		if (self.is_subcategory(category)):
			return self.server.softcat.getProductCount(category['parentID'],
													   category['id'],
													   self.distro)
		else:
			return self.server.softcat.getProductCount(category['id'],
													   self.distro)

	def get_package(self, packageName):
		return self.server.softcat.findPackageByName(packageName,
													 self.distro,
													 self.cpuArch)
		
    
                                               

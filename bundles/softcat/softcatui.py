import reef

from gtk import *
from gnome.xmhtml import *

import imp
def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

html_widget = reef_import_module("html_widget")
from html_widget import HTMLWidget

def get_scrolled_window():
	sw = GtkScrolledWindow()
	sw.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC)
	sw.show()
	return sw

class SoftcatUI(GtkVBox):
	def install_button_clicked(self, button, *args):
		self.client.do_install()

	def product_list_select_row(self, clist, row, column, event, *args):
		reef.report_load_underway()
		idle_add(self.show_product, clist, row, column, event, *args)

	def show_product(self, clist, row, column, event, *args):
		product_id = clist.get_row_data(row)
		product = self.client.get_product(product_id)
		installed_version = self.client.get_installed_version(product)
		available_version = self.client.get_available_version(product)
		is_update = self.client.is_update_available(product)
		self.client.set_current_product(product)
		self.product_pane.set_product(product,
									  installed_version,
									  available_version,
									  is_update)
		reef.report_load_complete()

	def category_tree_select_row(self, tree, node, column):
		category = tree.node_get_row_data(node)
		if self.client.is_subcategory(category):
			reef.report_load_underway()
			idle_add(self.show_product_list, tree, node, column)

	def show_product_list(self, tree, node, column):
		self.product_list.freeze()
		self.product_list.clear()
		subcategory = tree.node_get_row_data(node)
		for product in self.client.get_products(subcategory):
			update = ""
			if (self.client.is_update_available(product)):
				if (self.client.is_installed(product)):
					update = "U"
				else:
					update = "*"
			rownum = self.product_list.append([update,
											   product['name'],
											   product['summary']
											   ])
			self.product_list.set_row_data(rownum, product['id'])
		self.product_pane.clear_product()
		self.product_list.thaw()
		reef.report_load_complete()

	def category_tree_expand(self,tree,node):
		child = node.children[0]
		if (self.category_tree.node_get_row_data(child) == None):
			self.category_tree.freeze()
			self.category_tree.remove_node(child)
			category = self.category_tree.node_get_row_data(node)
			subcategories = category['subcategories']
			for subcat in subcategories:
				subnode = self.category_tree.insert_node(node, None, [subcat['title'],
															 str(self.client.get_product_count(subcat))])
				self.category_tree.node_set_row_data(subnode, subcat)
			self.category_tree.thaw()

	def show_category_list(self, *args):
		self.category_tree.freeze ()
		self.category_tree.clear ()
		for c in self.client.get_categories():
			node = self.category_tree.insert_node(None, None,
										 [c['title'],
										  str(self.client.get_product_count(c))],
										 is_leaf=FALSE)
			self.category_tree.node_set_row_data(node,c)
			self.category_tree.insert_node(node, None, ['placeholder',''])
		self.category_tree.set_expander_style(CTREE_EXPANDER_TRIANGLE)
		self.category_tree.set_line_style(CTREE_LINES_NONE)
		self.category_tree.thaw ()
		reef.report_load_complete()

	def __init__(self, client):
		self.client = client
		
		GtkVBox.__init__(self)
		self.set_homogeneous(FALSE)
		self.show()

		# Top pane: title label
		label = GtkLabel("Eazel Software Catalog")
		self.add(label)
		self.set_child_packing(label,FALSE,FALSE,10,PACK_START)
		label.show()

		# Lower pane: horizontal-split
		hpane = GtkHPaned()
		hpane.set_position(250)
		hpane.set_sensitive(TRUE)
		self.add(hpane)
		hpane.show()

		# Left pane: category hierarchy tree
		sw = get_scrolled_window()
		hpane.add(sw)

		# Category tree set to self.category_tree
		tree = GtkCTree(2)
		self.category_tree = tree
		sw.add(tree)
		tree.set_flags(CAN_FOCUS)
		tree.set_usize(-1,-1)
		tree.set_column_width(0, 180)
		tree.set_column_width(1, 65)
		tree.set_selection_mode(SELECTION_SINGLE)
		tree.set_shadow_type(SELECTION_SINGLE)

		tree.set_column_title(0,"Category")
		tree.set_column_title(1,"#")
		tree.column_titles_show()

		tree.show()

		# Right pane: horiz pane with cat list and product info
		vpane = GtkVPaned()
		vpane.set_position(50)
		vpane.show()
		hpane.add(vpane)

		sw = get_scrolled_window()
		vpane.add(sw)

		# Product list set to self.product_list
		list = GtkCList(3)
		self.product_list = list
		sw.add(list)

		list.set_column_title(1,"Product")
		list.set_column_title(2,"Summary")
		list.column_titles_show()
		list.show()

		# Product display pane set to self.product_pane
		prodpane = ProductPane()
		self.product_pane = prodpane
		vpane.add(prodpane)
		prodpane.show()

		# Connect event callbacks
		self.product_pane.install_button.connect("clicked",self.install_button_clicked)
		self.product_list.connect("select-row",self.product_list_select_row)
		self.category_tree.connect("tree-select-row",self.category_tree_select_row)
		self.category_tree.connect("tree-expand",self.category_tree_expand)

		reef.report_load_underway()
		idle_add(self.show_category_list)
 
class ProductPane(GtkVBox):
	def clear_product(self):
		self.product_name_label.set_text("")
		self.installed_version_label.set_text("")
		self.available_version_label.set_text("")
		self.description_box.set("")
		self.install_button.set_sensitive(FALSE)
		self.install_button_label.set_text("Install")
		
	def set_product(self, product, installedversion, availableversion, is_update):
		self.product_name_label.set_text(product['name'])
		self.description_box.set(product['description'])
		if installedversion != None:
			self.installed_version_label.set_text(installedversion)
		else:
			self.installed_version_label.set_text("Not installed")
		self.available_version_label.set_text(availableversion)

		if is_update > 0:
			self.install_button.set_sensitive(TRUE)
			if installedversion != None:
				self.install_button_label.set_text("Update")
			else:
				self.install_button_label.set_text("Install")
		else:
			self.install_button.set_sensitive(FALSE)
	
	def __init__(self):
		GtkVBox.__init__(self)
		self.set_homogeneous(FALSE)
		
		hbox = GtkHBox()
		hbox.set_homogeneous(FALSE)
		self.add(hbox)
		self.set_child_packing(hbox, FALSE, FALSE, 5, PACK_START)
		hbox.show()

		label = GtkLabel()
		self.product_name_label = label
		hbox.add(label)
		label.show()

		button = GtkButton()
		self.install_button = button
		hbox.add(button)
		hbox.set_child_packing(button, TRUE, FALSE, 5, PACK_END)
		button.show()
		button.set_sensitive(FALSE)

		label = GtkLabel("Install")
		self.install_button_label = label
		button.add(label)
		label.show()

		table = GtkTable(5,5,TRUE)
		self.add(table)
		table.show()

		html = HTMLWidget(None)
		self.description_box = html
		html.show()

		table.attach(html, 0, 5, 0, 3)

		label = GtkLabel("Installed Version:")
		table.attach(label, 0, 2, 3, 4)
		label.show()
		label = GtkLabel()
		self.installed_version_label = label
		table.attach(label, 3, 5, 3, 4)
		label.show()

		label = GtkLabel("Available Version:")
		table.attach(label, 0, 2, 4, 5)
		label.show()
		label = GtkLabel()
		self.available_version_label = label
		table.attach(label, 3, 5, 4, 5)
		label.show()

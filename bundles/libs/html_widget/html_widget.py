from gtk import *
from gtkhtml import *

class HTMLWidget (GtkScrolledWindow):
	def __init__(self, clicked_callback, get_content_callback = None):
		GtkScrolledWindow.__init__ (self)
		self.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC)
		self.html = GtkHTML()
		self.html.show ()
		self.add (self.html)
		self.html.load_empty()
		self.clicked_callback = clicked_callback
		self.get_content_callback = get_content_callback
		self.html.connect('url_requested', self._get_content)
		self.html.connect('link_clicked', self._activate)
		self.url = ''

	def set (self, html):
		handle = self.html.begin()
		self.html.write(handle, html)
		self.html.end(handle, HTML_STREAM_OK)

	def _activate (self, html, url):
		if self.clicked_callback:
			self.clicked_callback (self, url)

	def _get_content (self, html, url, handle):
		if not self.get_content_callback:
			self.html.end (handle, HTML_STREAM_ERROR)
			return

		content = self.get_content_callback (self, url)

		if content == None:
			self.html.end (handle, HTML_STREAM_ERROR)
			return

		self.html.write (handle, content)
		self.html.end(handle, HTML_STREAM_OK)

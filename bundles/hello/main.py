#!/usr/bin/python

from gtk import *
import reef

#from libhello import Hello

print "Hello world"

#h = Hello()

#def init_view (svb):
#	button = GtkButton('Hello, Cruel World')
#	button.show()
#	return button


#import reef

#print 'asdf'+`dir()`

vbox = GtkVBox (0, 0)
hbox = GtkHBox (0, 0)
hello = GtkLabel ('Hello World')
start = GtkButton ('start')
stop = GtkButton ('stop')

vbox.show()
hbox.show()
hello.show()
start.show()
stop.show()

vbox.add (hello)
vbox.add (hbox)
hbox.add (start)
hbox.add (stop)

def start_cb (*args):
	reef.report_load (0)

start.connect ('clicked', start_cb)

def stop_cb (*args):
	reef.report_load (1)

stop.connect ('clicked', stop_cb)

reef.get_container_widget ().add (vbox)
reef.set_title ('Hi there!')

#!/usr/bin/python

import imp
from gtk import *
import soap
import reef

def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

terraserviceui = reef_import_module("terraserviceui")

MAX_ITEMS=200

SCALE_DICT={'1mm': 'Scale1mm',
			'2mm': 'Scale2mm',
			'4mm': 'Scale4mm',
			'8mm': 'Scale8mm',
			'16mm': 'Scale16mm',
			'32mm': 'Scale32mm',
			'63mm': 'Scale63mm',
			'125mm': 'Scale125mm',
			'250mm': 'Scale250mm',
			'500mm': 'Scale500mm',
			'1m': 'Scale1m',
			'2m': 'Scale2m',
			'4m': 'Scale4m',
			'8m': 'Scale8m',
			'16m': 'Scale16m',
			'32m': 'Scale32m',
			'64m': 'Scale64m',
			'128m': 'Scale128m',
			'256m': 'Scale256m',
			'512m': 'Scale512m',
			'1km': 'Scale1km',
			'2km': 'Scale2km',
			'4km': 'Scale4km',
			'8km': 'Scale8km',
			'16km': 'Scale16km'}

THEME_DICT={'Photograph': 'Photo',
			'Topographical': 'Topo',
			'Relief': 'Relief'}

class TerraserviceClient:
	def __init__(self):
		self.ui = terraserviceui.TerraserviceUI(self,THEME_DICT,SCALE_DICT)
		self.server = soap.SOAPProxy('http://terraserver.microsoft.net/TerraService.asmx?SDL','sdl')

	def search_for_place(self, search_text, set_results):
		results = self.server.GetPlaceList(placeName=search_text,
										   MaxItems=MAX_ITEMS,
										   imagePresence=0)
		if (results.has_key('PlaceFacts')):
			set_results(results['PlaceFacts'])
		else:
			set_results([])
		
	def get_image(self, item_data, theme, scale, set_image):
		meta = self.server.GetTileMetaFromLonLatPt(point=item_data['Center'],
											 theme=theme, scale=scale)
		tileid = meta['Id']
		set_image(self.server.GetTile(id=tileid))

	def load_external_link (self, widget, href):
		#os.system('gnome-moz-remote --newwin '+href)
		reef.open_location_prefer_existing_window (href)
		
reef.get_container_widget().add(TerraserviceClient().ui.widget)

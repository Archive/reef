import reef

from gtk import *

import libglade
import GdkImlib
import os

import imp
def reef_import_module(modname):
	[file, pathname, description] = imp.find_module(modname, [reef.get_bundle_base()])
	ret = imp.load_module(modname,file, pathname, description)
	file.close()
	return ret

class TerraserviceUI:
	def search_button_clicked(self, button, *args):
		reef.report_load(0)
		entry_text = self.search_entry.get_text()
		idle_add(self.client.search_for_place, entry_text, self.set_results)

	def set_results(self, results):
		self.result_list.freeze()
		self.result_list.clear()
		for item in results:
			row = self.result_list.append([item['Place']['City'],item['Place']['State'],item['Place']['Country'],item['PlaceTypeId']])
			self.result_list.set_row_data(row, item)
		self.result_list.thaw()
		reef.report_load(1)

	def menu_item_activate(self, menuitem):
		if self.curritem_data is not None:
			reef.report_load(0)
			theme = self.theme_menu.get_active().get_data("value")
			scale = self.scale_menu.get_active().get_data("value")
			print "Theme",theme
			print "Scale",scale
		
			idle_add(self.client.get_image, self.curritem_data, theme, scale,
					 self.set_image)
			

	def result_list_select_row(self, clist, row, column, event, *args):
		reef.report_load(0)
		self.curritem_data = clist.get_row_data(row)
		theme = self.theme_menu.get_active().get_data("value")
		scale = self.scale_menu.get_active().get_data("value")
		print "Theme",theme
		print "Scale",scale
		
		idle_add(self.client.get_image, self.curritem_data, theme, scale,
				 self.set_image)

	def set_image(self, image_data):
		filename = os.tmpnam()
		file = open(filename, 'w')
		file.write(image_data)
		file.close()
		imgdata = GdkImlib.Image(filename)
		imgdata.render()
		if self.image is not None:
			self.image_port.remove(self.image)
		self.image = imgdata.make_pixmap()
		self.image_port.add(self.image)
		self.image.show()
		reef.report_load(1)

	def populate_menu(self, menu, dict, callback):
		keys = dict.keys()
		keys.sort()
		for key in keys:
			item = GtkMenuItem(key)
			item.set_data("value",dict[key])
			item.show()
			item.connect("activate",callback)
			menu.append(item)
		
	def __init__(self, client, themedict, scaledict):
		self.client = client
		
		xml = libglade.GladeXML(reef.get_bundle_base() + "/terraservice.glade", "main_pane")
		self.search_button = xml.get_widget("search_button")
		main_pane = xml.get_widget("main_pane")
		self.widget = main_pane
		
		self.search_entry = xml.get_widget("search_entry")
		self.result_list = xml.get_widget("result_list")

		theme_optionmenu = xml.get_widget("theme_menu")
		menu = GtkMenu()
		self.populate_menu(menu, themedict, self.menu_item_activate)
		menu.set_active(0)
		menu.show()
		theme_optionmenu.set_menu(menu)
		self.theme_menu = menu
		
		scale_optionmenu = xml.get_widget("scale_menu")
		menu = GtkMenu()
		self.populate_menu(menu, scaledict, self.menu_item_activate)
		menu.set_active(0)
		menu.show()
		scale_optionmenu.set_menu(menu)
		self.scale_menu = menu

		self.curritem_data = None

		self.image_port = xml.get_widget("image_port")
		self.image = None
		# Connect event callbacks
		self.search_button.connect("clicked",self.search_button_clicked)
		self.search_entry.connect("activate",self.search_button_clicked)
		self.result_list.connect("select-row",self.result_list_select_row)

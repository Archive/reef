#include <libreef/libreef.h>
#include <libgnomevfs/gnome-vfs.h>
#include <glib.h>

int main (int argc, char **argv) {
	ReefSVB *svb;

	if (argc != 2) {
		g_print ("%s <svb uri>\n", argv[0]);
		return 1;
	}

	gnome_vfs_init ();

	svb = reef_svb_download (argv[1]);

	g_assert (svb != NULL);

	g_print ("URI = \"%s\"\n", reef_svb_get_uri (svb));
	g_print ("Unpacked base = \"%s\"\n", reef_svb_get_base (svb));

	reef_svb_free (svb);

	return 0;
}

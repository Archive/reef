/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2001 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ian McKellar <ian@eazel.com>
 */

/* reef-svb-view.c - 
 */

#include <config.h>
#include <libreef/libreef.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gtk/gtk.h>


/* callbacks */
static void test_set_title                 (const char       *title,
		                            gpointer         data);
static void test_open_location             (const char       *uri,
		                            gboolean         new_window,
		                            gpointer         data);
static void test_report_status             (const char       *uri,
		                            gpointer         data);
static void test_report_load               (gboolean         complete,
		                            gpointer         data);
     
static ReefCallbacks callbacks = {
	&test_set_title,
	&test_open_location,
	&test_report_status,
	&test_report_load
};


int main (int argc, char **argv) {
	GtkWidget *container;
	GtkWidget *window;
	ReefEnvironment *environment;
	ReefSVB *svb;

	if (argc != 2) {
		g_print ("%s <svb uri>\n", argv[0]);
		return 1;
	}

	gtk_init (&argc, &argv);
	gnome_vfs_init ();

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GTK_OBJECT(window), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	container = gtk_event_box_new ();
	gtk_container_add (GTK_CONTAINER (window), container);
	gtk_widget_show (container);
	gtk_widget_show (window);

	svb = reef_svb_download (argv[1]);

	g_print ("svb is downloaded...\n");

	environment = reef_environment_new (GTK_EVENT_BOX (container), svb,
			&callbacks, NULL);

	g_print ("environment is created...\n");

	gtk_main ();

	return 0;

}

static void test_set_title                 (const char       *title,
					    gpointer         data)
{
	g_print ("TEST: title `%s'\n", title);
}


static void test_open_location             (const char       *uri,
		                            gboolean         new_window,
					    gpointer         data)
{
	if (new_window) {
		g_print ("TEST: open `%s'\n", uri);
	} else {
		g_print ("TEST: open `%s' (new window)\n", uri);
	}
}

static void test_report_status             (const char       *status,
					    gpointer         data)
{
	g_print ("TEST: status `%s'\n", status);
}

static void test_report_load               (gboolean         complete,
					    gpointer         data)
{
	if (complete) {
		g_print ("TEST: load complete\n");
	} else {
		g_print ("TEST: load incomplete\n");
	}
}
     

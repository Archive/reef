#!/usr/bin/python

# a local SVB loader

from gtk import *
from gnome.ui import *
from reef import *
import sys

if len(sys.argv) != 2:
	print '%s <svb path name>' % sys.argv[0]
	sys.exit ()

svb = SVB (sys.argv[1])

win = GtkWindow ()
win.connect ('destroy', mainquit)
win.show ()

win.add(svb.init_view ())

mainloop ()

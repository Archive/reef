# Reef metadata
# Exposes reef metadata as defined in SVB XML DTD

import xml.sax
import xml.sax.handler
import StringIO

class ReefMetadata:
    def __init__(self,filename_or_stream):
        self.handler = self.gethandler()
        xml.sax.parse(filename_or_stream, self.handler)

    def gethandler(self):
        return ReefMetadataHandler(self)

METADATATAGS = ['name','icon','summary','description',
                'publisher','author','copyright']

class ReefMetadataHandler(xml.sax.handler.ContentHandler):
    # return a text representation of this tag
    def getTag(self, name, attrs):
        attrNames = attrs.getNames()
        buf = StringIO.StringIO()
        buf.write("<")
        buf.write(name)
        for aname in attrNames:
            buf.write(" ")
            buf.write(aname)
            buf.write('="')
            buf.write(attrs.getValue(aname))
            buf.write('"')
        buf.write(">")
        ret = buf.getvalue()
        buf.close()
        return ret
    
    def __init__(self, metadata):
        self._metadata = metadata
        self._context = None
        self._map = {}
    
    def startElement(self, name, attrs):
        if name == 'svb':
            self._metadata.env_version = attrs.getValue('env-version')
        elif name == 'implementation':
            self._metadata.language = attrs.getValue('language')
            self._metadata.codepath = attrs.getValue('codepath')
        elif name in METADATATAGS:
            self._context = name
        else:
            self.characters(self.getTag(name,attrs))

    def endElement(self, name):
        if name in METADATATAGS:
            self._context = None
        else:
            self.characters("</" + name + ">")

    def characters(self, content):
        if self._context != None:
            if self._map.has_key(self._context):
                oldcontent = self._map[self._context]
                self._map[self._context] = oldcontent + content
            else:
                self._map[self._context] = content

    def endDocument(self):
        self._metadata.name = self._map['name']
        self._metadata.icon = self._map['icon']
        self._metadata.summary = self._map['summary']
        self._metadata.copyright = self._map['copyright']
        if self._map.has_key('description'):
            self._metadata.description = self._map['description']
        if self._map.has_key('publisher'):
            self._metadata.publisher = self._map['publisher']
        if self._map.has_key('author'):
            self._metadata.author = self._map['author']

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Maciej Stachowiak <mjs@eazel.com>
 *          Ian McKellar <ian@eazel.com>
 */

/* main.c - Main function and object activation function for Reef SVB
 * view component.
 */

#include <config.h>
#include "reef-svb-view.h"
#include <libnautilus/nautilus-view-standard-main.h>
#include <Python.h>

#define FACTORY_IID     "OAFIID:reef_svb_nautilus_view_factory:09f6e21c-a094-418c-a065-29ad1bdf2774"
#define VIEW_IID        "OAFIID:reef_svb_nautilus_view:4719f746-8354-48b3-8d2b-29c0b2418605"
#define EXECUTABLE_NAME "reef-svb-nautilus-view"
#define GET_TYPE_FUNCTION reef_svb_view_get_type

int
main (int argc, char *argv[])
{
	return nautilus_view_standard_main (EXECUTABLE_NAME, VERSION,
					    NULL /* gettext_package_name */,
					    NULL /* gettext_locale_directory */,
					    argc, argv,
					    FACTORY_IID, VIEW_IID,
					    nautilus_view_create_from_get_type_function,
					    NULL /* post-intialize callback */,
					    GET_TYPE_FUNCTION);
	Py_Exit(0);
	/*NOTREACHED*/
}

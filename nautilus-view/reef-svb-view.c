/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2001 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com>
 */

/* reef-svb-view.c - 
 */

#include <config.h>
#include "reef-svb-view.h"

#include <pwd.h>
#include <libnautilus/nautilus-bonobo-ui.h>
#include <bonobo/bonobo-control.h>
#include <libgnome/gnome-i18n.h>
#include <gtk/gtklabel.h>
#include <gtk/gtksignal.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "libreef/libreef.h"


struct ReefSVBViewDetails {
	ReefSVB         *svb;
	ReefEnvironment *environment;
	GtkWidget       *container;
};

static void reef_svb_view_initialize_class (ReefSVBViewClass *klass);
static void reef_svb_view_initialize       (ReefSVBView      *view);
static void reef_svb_view_destroy          (GtkObject        *object);
static void load_location_callback         (NautilusView     *nautilus_view,
					    const char       *location,
					    gpointer          user_data);

/* callbacks */
static void reef_set_title                 (const char       *title,
		                            gpointer         data);
static void reef_open_location             (const char       *uri,
		                            gboolean         new_window,
		                            gpointer         data);
static void reef_report_status             (const char       *uri,
		                            gpointer         data);
static void reef_report_load               (gboolean         complete,
		                            gpointer         data);
     

static gpointer parent_class;

static ReefCallbacks callbacks = {
	&reef_set_title,
	&reef_open_location,
	&reef_report_status,
	&reef_report_load
};
                                                                                                        
GtkType
reef_svb_view_get_type (void)                                                         
{                                                                                                       
	GtkType parent_type;                                                                            
	static GtkType type;                                                                            
                                                                                                        
	if (type == 0) {                                                                                
		static GtkTypeInfo info = {
		        "ReefSVBView",
			sizeof (ReefSVBView),
			sizeof (ReefSVBViewClass),
			(GtkClassInitFunc)reef_svb_view_initialize_class,
			(GtkObjectInitFunc)reef_svb_view_initialize,
			NULL,
			NULL,
			NULL
		};

		parent_type = (NAUTILUS_TYPE_VIEW);
		type = gtk_type_unique (parent_type, &info);
		parent_class = gtk_type_class (parent_type);
	}

	return type;
}

static void
reef_svb_view_initialize_class (ReefSVBViewClass *klass)
{
	GtkObjectClass *object_class;
	
	g_assert (REEF_IS_SVB_VIEW_CLASS (klass));

	object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = reef_svb_view_destroy;
}

static void
reef_svb_view_initialize (ReefSVBView *view)
{
	GtkWidget *container;

	g_assert (REEF_IS_SVB_VIEW (view));

	view->details = g_new0 (ReefSVBViewDetails, 1);
	
	container = gtk_event_box_new ();
	gtk_widget_show (container);
	gtk_object_ref (GTK_OBJECT (container));
	view->details->container = container;
	view->details->environment = 
		reef_environment_new (GTK_EVENT_BOX (container), NULL,
				&callbacks, view);
	
	nautilus_view_construct (NAUTILUS_VIEW (view), 
				 view->details->container);
	
	gtk_signal_connect (GTK_OBJECT (view), 
			    "load_location",
			    load_location_callback, 
			    NULL);
}

static void
reef_svb_view_destroy (GtkObject *object)
{
	ReefSVBView *view;
	
	view = REEF_SVB_VIEW (object);
	
	reef_environment_free (view->details->environment);
	gtk_object_unref (GTK_OBJECT (view->details->container));

	g_free (view->details);

	(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
load_location (ReefSVBView *view,
	       const char *location)
{
	ReefEnvironment *env;
	
	g_assert (REEF_IS_SVB_VIEW (view));
	g_assert (location != NULL);

	env = view->details->environment;
	
	reef_environment_free (env);

	env = reef_environment_new (GTK_EVENT_BOX (view->details->container),
			reef_svb_download (location), &callbacks, view);

	view->details->environment = env;
	
	
}

static void
load_location_callback (NautilusView *nautilus_view, 
			       const char *location,
			       gpointer user_data)
{
	ReefSVBView *view;
	
	g_assert (NAUTILUS_IS_VIEW (nautilus_view));
	g_assert (location != NULL);
	
	view = REEF_SVB_VIEW (nautilus_view);
	
	nautilus_view_report_load_underway (nautilus_view);
	
	/* Do the actual load. */
	load_location (view, location);
	
	nautilus_view_report_load_complete (nautilus_view);
}


static void reef_set_title                 (const char       *title,
					    gpointer         data)
{
	ReefSVBView *view = REEF_SVB_VIEW (data);

	nautilus_view_set_title (NAUTILUS_VIEW (view), title);
}


static void reef_open_location             (const char       *uri,
		                            gboolean         new_window,
					    gpointer         data)
{
	ReefSVBView *view = REEF_SVB_VIEW (data);

	if (new_window) {
		nautilus_view_open_location_prefer_existing_window (
				NAUTILUS_VIEW (view), uri);
	} else {
		nautilus_view_open_location_in_this_window (
				NAUTILUS_VIEW (view), uri);
	}
}

static void reef_report_status             (const char       *uri,
					    gpointer         data)
{
	ReefSVBView *view = REEF_SVB_VIEW (data);

	nautilus_view_report_status (NAUTILUS_VIEW (view), uri);
}

static void reef_report_load               (gboolean         complete,
					    gpointer         data)
{
	ReefSVBView *view = REEF_SVB_VIEW (data);

	if (complete) {
		nautilus_view_report_load_complete (NAUTILUS_VIEW (view));
	} else {
		nautilus_view_report_load_underway (NAUTILUS_VIEW (view));
	}
}
     

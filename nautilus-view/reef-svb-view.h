/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com>
 */

/* reef-svb-view.h - 
 */

#ifndef REEF_SVB_VIEW_H
#define REEF_SVB_VIEW_H

#include <gtk/gtklabel.h>
#include <libnautilus/nautilus-view.h>

#define REEF_TYPE_SVB_VIEW	      (reef_svb_view_get_type ())
#define REEF_SVB_VIEW(obj)	      (GTK_CHECK_CAST ((obj), REEF_TYPE_SVB_VIEW, ReefSVBView))
#define REEF_SVB_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), REEF_TYPE_SVB_VIEW, ReefSVBViewClass))
#define REEF_IS_SVB_VIEW(obj)	      (GTK_CHECK_TYPE ((obj), REEF_TYPE_SVB_VIEW))
#define REEF_IS_SVB_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), REEF_TYPE_SVB_VIEW))

typedef struct ReefSVBViewDetails ReefSVBViewDetails;

typedef struct {
	NautilusView parent;
	ReefSVBViewDetails *details;
} ReefSVBView;

typedef struct {
	NautilusViewClass parent;
} ReefSVBViewClass;

/* GtkObject support */
GtkType       reef_svb_view_get_type          (void);

#endif /* REEF_SVB_VIEW_H */

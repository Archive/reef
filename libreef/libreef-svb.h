#ifndef LIBREEF_SVB_H
#define LIBREEF_SVB_H

typedef struct {
	char *uri;
	char *unpacked_base;
} ReefSVB;


ReefSVB *reef_svb_download (const char *uri);
const char *reef_svb_get_uri (ReefSVB *svb);
const char *reef_svb_get_base (ReefSVB *svb);
void reef_svb_free (ReefSVB *svb);

#endif

#include "libreef/libreef.h"
#include <pygtk/pygtk.h>

static GHashTable *interpreter_to_environment_map = NULL;
static gboolean reef_is_initialized = FALSE;

static void reef_init (void) {
	static char *progname = "reef";
	if (!reef_is_initialized) {
	        /* Pass argv[0] to the Python interpreter */
	        Py_SetProgramName(progname);

	        /* Initialize the Python interpreter.  Required. */
	        Py_Initialize();

		if (!gnome_vfs_initialized ()) {
			gnome_vfs_init ();
		}
		reef_is_initialized = TRUE;
	}
}


static ReefEnvironment *
get_current_environment (void)
{
	ReefEnvironment *hash_value;
	
	hash_value = g_hash_table_lookup (interpreter_to_environment_map,
					  PyThreadState_Get ());

	return hash_value;
}


/* The Reef Python module */

static PyObject *
reef_get_uri (PyObject *self,
	      PyObject *args)
{
	return PyString_FromString (reef_svb_get_uri(
				get_current_environment ()->svb));
}


static PyObject *
reef_get_container_widget (PyObject *self,
			   PyObject *args)
{
	Py_INCREF (get_current_environment ()->wrapped_container);
	return get_current_environment ()->wrapped_container;
}


static PyObject *
reef_get_bundle_base (PyObject *self,
		      PyObject *args)
{
	return PyString_FromString (reef_svb_get_base(
				get_current_environment ()->svb));
}

static PyObject *
reef_set_title (PyObject *self,
                PyObject *args)
{
	char *title;
	ReefEnvironment *env;

	if (!PyArg_ParseTuple(args, "s", &title)) {
		return NULL;
	}

	env = get_current_environment ();
	(env->callbacks->set_title)(title, env->user_data);

	Py_INCREF(Py_None);
        return Py_None;
}

static PyObject *
reef_report_status (PyObject *self,
                    PyObject *args)
{
	char *status;
	ReefEnvironment *env;

	if (!PyArg_ParseTuple(args, "s", &status)) {
		return NULL;
	}

	env = get_current_environment ();
	(env->callbacks->report_status)(status, env->user_data);

	Py_INCREF(Py_None);
        return Py_None;
}

static PyObject *
reef_report_load (PyObject *self,
		  PyObject *args)
{
	int complete;
	ReefEnvironment *env;

	if (!PyArg_ParseTuple(args, "i", &complete)) {
		return NULL;
	}

	env = get_current_environment ();
	(env->callbacks->report_load)(complete, env->user_data);

	Py_INCREF(Py_None);
        return Py_None;
}

static PyObject *
reef_open_location (PyObject *self,
		    PyObject *args)
{
	char *location;
	int new_window;
	ReefEnvironment *env;

	if (!PyArg_ParseTuple(args, "si", &location, &new_window)) {
		return NULL;
	}

	env = get_current_environment ();
	(env->callbacks->open_location)(location, new_window, env->user_data);

	Py_INCREF(Py_None);
        return Py_None;
}

static PyMethodDef ReefMethods[] = {
	/* methods provided by this library */
	{"get_uri", reef_get_uri, METH_VARARGS},
	{"get_container_widget", reef_get_container_widget, METH_VARARGS},
	{"get_bundle_base", reef_get_bundle_base, METH_VARARGS},

	/* methods provided by the "host" */
	{"set_title", reef_set_title, METH_VARARGS},
	{"open_location", reef_open_location, METH_VARARGS},
	{"report_status", reef_report_status, METH_VARARGS},
	{"report_load", reef_report_load, METH_VARARGS},
	{NULL, NULL}
};

static PyObject *
wrap_event_box (GtkWidget *event_box)
{
	PyObject *gtk_module;
	PyObject *obj;
	PyObject *event_box_constructor;
	PyObject *string;
	PyObject *call_args;
	PyObject *wrapped_obj;
	GtkObject *gtkobj;

	gtkobj = GTK_OBJECT (event_box);
	obj = PyGtk_New (gtkobj);

	gtk_module = PyImport_ImportModule ("gtk");
	string = PyString_FromString ("GtkEventBox");
	event_box_constructor = PyDict_GetItem (PyModule_GetDict (gtk_module),
						string);
	if (!PyCallable_Check (event_box_constructor)) {
		/* FIXME: should handle this better */ 
		return NULL;
	}

	call_args = Py_BuildValue ("(O)", obj);

	wrapped_obj = PyEval_CallObject (event_box_constructor, call_args);

	Py_DECREF (string);
	Py_DECREF (obj);
	Py_DECREF (call_args);
	Py_DECREF (gtk_module);
	
	return wrapped_obj;
}

/* actually run the script */
static int
run_reef_script (ReefEnvironment *env)
{
	FILE *fp;
	char *script_path;

	if (env->svb != NULL) {
		script_path = g_strdup_printf("%s/%s", 
				reef_svb_get_base(env->svb),
				"main.py"); /* FIXME get scriptname from xml */

		g_print ("[REEF] gonna run script `%s'\n", script_path);
		
		fp = fopen (script_path, "r");
		
		PyRun_SimpleFile (fp, (char *) script_path); 

		g_free (script_path);

		fclose (fp);

	}

	return FALSE;
}

ReefEnvironment *reef_environment_new (GtkEventBox *container, 
		                       ReefSVB *svb,
				       ReefCallbacks *callbacks,
				       gpointer user_data)
{
	static char *fake_argv[] = {"reef-environment"};
	ReefEnvironment *env = g_new0 (ReefEnvironment, 1);

	reef_init ();

	env->interpreter = Py_NewInterpreter ();

	PySys_SetArgv (0, fake_argv);

	init_pygtk ();

	env->container = GTK_WIDGET(container);
	env->wrapped_container = wrap_event_box (GTK_WIDGET (container));

	gtk_object_ref (GTK_OBJECT (container));

	Py_InitModule("reef", ReefMethods);

	env->svb = svb;

	if (interpreter_to_environment_map == NULL) {
		interpreter_to_environment_map = 
			g_hash_table_new (g_direct_hash, g_direct_equal);
		/* FIXME must destroy when appropriate */
	} 

	env->callbacks = callbacks;
	env->user_data = user_data;

	g_hash_table_insert (interpreter_to_environment_map, env->interpreter, 
			env);

	if (env->svb != NULL) {
		gtk_idle_add ((GtkFunction)run_reef_script, env);
	}

	return env;
}


PyThreadState *reef_environment_get_interpreter (ReefEnvironment *env)
{
	return env->interpreter;
}

GtkWidget *reef_environment_get_container (ReefEnvironment *env)
{
	return env->container;
}

static void
x_gtk_container_remove (GtkWidget *widget, 
			GtkWidget *container)
{
	gtk_container_remove (GTK_CONTAINER (container), widget); 
}

void reef_environment_free (ReefEnvironment *env)
{
	if (env == NULL) {
		return;
	}

	gtk_container_foreach (GTK_CONTAINER (env->container),
			       (GtkCallback) x_gtk_container_remove,
			       env->container);

	Py_XDECREF (env->wrapped_container);
	gtk_object_unref (GTK_OBJECT (env->container));
	Py_EndInterpreter (env->interpreter);
	
	/* TODO: remove unpacked bundle? */
	/* how to refcount this? */

	if (env->svb != NULL) {
		reef_svb_free (env->svb);
	}

	g_hash_table_remove (interpreter_to_environment_map, env->interpreter);

	g_free (env);
}

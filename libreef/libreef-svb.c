
#include <libgnomevfs/gnome-vfs.h>
#include <pwd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "libreef.h"



ReefSVB *reef_svb_download (const char *uri) {
	ReefSVB *svb = g_new0 (ReefSVB, 1);
	GnomeVFSURI *vfsuri;
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	FILE *localfile;
	char *path;
	char *escaped_uri;
	char *cachename;
	char *buf;
	GnomeVFSFileSize bytes, bytes_read;
	struct passwd *pwent;
	pid_t pid;
	int exit_status;
	int dev_null_fd;
	gboolean cached = FALSE;

	svb->uri = g_strdup (uri);


	vfsuri = gnome_vfs_uri_new (uri);

	if (vfsuri == NULL) {
		g_error ("Couldn't parse URI: %s\n", uri);
		g_free (svb->uri);
		g_free (svb);
		return NULL;
	}

	if (gnome_vfs_uri_is_local (vfsuri)) {
		path = g_strdup (gnome_vfs_uri_get_path (vfsuri));
		/* FIXME: check */
		gnome_vfs_uri_unref (vfsuri);
		cachename = path;
		cached = FALSE;
	} else {
		result = gnome_vfs_open_uri (&handle, vfsuri, GNOME_VFS_OPEN_READ);

		if (result != GNOME_VFS_OK) {
			g_error ("gnome_vfs_open_uri failed (%s)\n",
					gnome_vfs_result_to_string (result));
			g_free (svb->uri);
			g_free (svb);
			return NULL;
		}

		gnome_vfs_uri_unref (vfsuri);

		escaped_uri = gnome_vfs_escape_string (uri);
		cachename = g_strdup_printf ("/tmp/reef-svb-download-%s", 
				escaped_uri);
		g_free (escaped_uri);
		localfile = fopen (cachename, "w");

		if (localfile == NULL); {
			g_error ("Couldn't open local file: %s\n", cachename);
			gnome_vfs_close (handle);
			g_free (cachename);
			g_free (svb->uri);
			g_free (svb);
			return NULL;
		}

		bytes = 4096;
		buf = g_malloc (bytes);

		for (;;) {
			/* FIXME: this should really be async */
			bytes_read = 0;
			result = gnome_vfs_read (handle, buf, bytes, &bytes_read);
			if (result != GNOME_VFS_OK && bytes_read == 0) {
				break;
			}
			fwrite (buf, 1, bytes_read, localfile);
		}

		gnome_vfs_close (handle);
		fclose (localfile);
		g_free (buf);
		cached = TRUE;
	}

	/* work out where we're going to put the unpacked SVB */
	pwent = getpwuid (getuid());
	escaped_uri = gnome_vfs_escape_string (uri);
	svb->unpacked_base = 
		g_strdup_printf("%s/.reef/unpacked-svb-cache/%s", 
				pwent->pw_dir, escaped_uri);

	g_free (escaped_uri);

	pid = fork ();
	if (!pid) {
		/* child */
		/* ensure directory exists [hack] */

		if (chdir (pwent->pw_dir) == -1) {
			g_error ("changing to home directory (%s)\n", 
					strerror (errno));
		}

		/* ignore mkdir errors */
		mkdir (".reef", 0700);
		mkdir (".reef/unpacked-svb-cache", 0700);
		mkdir (svb->unpacked_base, 0700);

		if (chdir (svb->unpacked_base) == -1) {
			g_error ("error changing directories to %s (%s)\n",
					svb->unpacked_base, strerror (errno));
		}
#if 0	
		dev_null_fd = open ("/dev/null", O_RDONLY);
		close (0);
		dup2 (dev_null_fd, 0);
#endif
		execlp ("unzip", "unzip", "-u", "-o", cachename, NULL);
		g_print ("exec failed...");
		close (dev_null_fd);
		_exit (1);
	}


	g_print ("parent sleeps...\n");
	/* parent */
	waitpid (pid, &exit_status, 0);
	g_print ("parent wakes...\n");

	/* FIXME: check exit status */

	/* FIXME: free pwent */

	if (cached) {
		/* FIXME: remove cachename */
	}

	return svb;
}

const char *reef_svb_get_uri (ReefSVB *svb) {
	return svb->uri;
}

const char *reef_svb_get_base (ReefSVB *svb) {
	return svb->unpacked_base;
}

void reef_svb_free (ReefSVB *svb) {
	/* FIXME - remove downloaded files */

	g_free (svb->uri);
	g_free (svb->unpacked_base);

	g_free (svb);
}

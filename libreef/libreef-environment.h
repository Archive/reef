#ifndef LIBREEF_ENVIRONMENT_H
#define LIBREEF_ENVIRONMENT_H

#include <Python.h>
#include <gtk/gtk.h>

typedef struct {
	void (*set_title) (const char *title, gpointer data);
	void (*open_location) (const char *uri, gboolean new_window, gpointer data);
	void (*report_status) (const char *title, gpointer data);
	void (*report_load) (gboolean complete, gpointer data);
} ReefCallbacks;

typedef struct {
	ReefSVB       *svb;
	ReefCallbacks *callbacks;
	PyThreadState *interpreter;
	GtkWidget     *container;
	PyObject      *wrapped_container;
	gpointer       user_data;
} ReefEnvironment;

ReefEnvironment *reef_environment_new (GtkEventBox *container, ReefSVB *svb,
		ReefCallbacks *callbacks, gpointer user_data);
PyThreadState *reef_environment_get_interpreter (ReefEnvironment *env);
GtkWidget *reef_environment_get_container (ReefEnvironment *env);
gpointer *reef_environment_get_data (ReefEnvironment *env);
void reef_environment_free (ReefEnvironment *env);


ReefEnvironment *reef_environment_get_by_data (gpointer user_data);

#endif


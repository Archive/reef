
import thread
import gtk

def _apply_async_callback (callback_func, retval, callback_args):
	apply (callback_func, (retval,) + callback_args)

def _apply_async_thread (body_func, body_args, callback_func, callback_args):
	print '_apply_async_thread'
	retval = apply (body_func, body_args)
	gtk.idle_add (_apply_async_callback, callback_func, retval, callback_args)

def apply_async (body_func, body_args, callback_func, callback_args):
	t = thread.start_new_thread (_apply_async_thread, (body_func, body_args, callback_func, callback_args))
	print 'got: '+`t`


## A wrapper to make this more useful within the reef framework.
reef = None
try:
	import reef
except:
	pass

def _reef_async_complete (*garbage):
	if reef:
		reef.report_load_underway ()

def reef_async (func, *args):
	if reef:
		reef.report_load_underway ()

	gtk.idle_add (apply_async, func, argc, _reef_async_complete, [])


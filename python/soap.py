import string, time, re
import urllib, xmllib, httplib
from types import *
from cgi import escape
from StringIO import StringIO

try:
	import sgmlop
except ImportError:
	sgmlop = None # accelerator not available

NAMESPACES = {"http://www.w3.org/1999/XMLSchema": "xsd",
			  "http://www.w3.org/1999/XMLSchema/instance/": "xsi",
			  "http://schemas.xmlsoap.org/soap/envelope/": "soap",
			  "http://schemas.xmlsoap.org/soap/encoding/": "enc",
			  "http://www.pythonware.com/soap/": "lab"}
# Standard base type namespace
NS_XSD = "http://www.w3.org/1999/XMLSchema"
# Standard encoding namespace
NS_ENC = "http://schemas.xmlsoap.org/soap/encoding/"
# Pythonware "lab" namespace
NS_LAB = "http://www.pythonware.com/soap/"


# check if a string is valid XML tag
_is_valid_tag = re.compile("^[a-zA-Z_][-a-zA-Z0-9._]*$").match

# version
__version__ = '0.1'
# user agent
__user_agent__ = 'soap.py/%s (Reef SOAP)' % __version__

def request(protocol, host, uri_path, soap_action, request_body):
	"""Executes a request to the SOAP server"""

	h = None
	if (protocol == 'https'):
		h = httplib.HTTPSConnection(host)
	else:
		h = httplib.HTTPConnection(host)
	h.putrequest("POST", uri_path)
	h.putheader("User-Agent", __user_agent__)
	h.putheader("Content-Type", "text/xml")
	h.putheader("Content-Length", str(len(request_body)))
	h.putheader("SOAPAction", soap_action)
	h.endheaders()

	h.send(request_body)

	# Kludge to handle spurious "100 Continue" responses from IIS
	response = None
	while 1:
		response = h.getresponse()
		if response.status != 100:
			break
		h._HTTPConnection__state = httplib._CS_REQ_SENT
		h._HTTPConnection__response = None

	return response

# Representations of XML Schema types
class ComplexType:
	"""Represents an XML Schema 'complex' type, which is a struct made up of simple types."""

	def __init__(self, name, namespace, items=None):
		self.name = name
		self.namespace = namespace
		self.members = {}
		if items is not None:
			for key in items.keys():
				self.members[namespace + key] = items[key]

	def addMember(self, namespace, name, type):
		self.members[namespace + name] = type

	def checkType(self, input):
		for key in self.members.keys():
			self.members[key].checkType(input[key[len(self.namespace):]])

	def writeXml(self, out, paramname, input, nscount):
		"""Writes the XML for this type to the output.  Returns the input
		nscount plus 1 since it uses this namespace."""
		out.write('<' + paramname + ' xmlns:q' + str(nscount) + '="' +
				self.namespace + '" type="q' +
				str(nscount) + ':' +
				self.name + '">')
		nscount = nscount + 1
		for k in input.keys():
			paramtype = self.members[self.namespace + k]
			nscount = paramtype.writeXml(out,k,input[k],nscount)
		out.write('</' + paramname + '>\n')
		return nscount + 1
			
class ArrayType:
	"""Represents a type which is actually another type which may occur
	multiple times."""

	def __init__(self, actualtype, min=0, max=None):
		self.actualtype = actualtype
		if type(actualtype) is not StringType:
			self.setType(actualtype)
		self.min = min
		self.max = max

	def setType(self, actualtype):
		self.name = actualtype.name
		self.namespace = actualtype.namespace
		self.members = actualtype.members

	def checkType(self, input):
		for item in input:
			self.actualtype.checkType(item)

	def writeXml(self, out, paramname, input, nscount):
		for item in input:
			self.actualtype.writeXml(out, paramname, item, nscount)
			nscount = nscount + 1
		return nscount

class SimpleType:
	"""Represents an XML Schema 'simple' type, which is derived from a base
	type.  It may be an enumerated type."""
	trans_dispatch = {}
	python_types = {}
	
	def __init__(self, name, namespace, base_type=None, base_namespace=None):
		self.name = name
		self.namespace = namespace
		self.base_type = base_type
		self.base_namespace = base_namespace
		if base_namespace is None or base_type is None:
			self.base_namespace = namespace
			self.base_type = name

		self.enumeration = None

	def addEnumElement(self, element):
		if self.enumeration is None:
			self.enumeration = []
		self.enumeration.append(element)

	# FIXME: the below doesn't work with non-simple types like binary
	def checkType(self, input):
		if not (type(input).__name__ ==
				self.python_types[self.base_namespace + self.base_type]):
			raise TypeError(input + " does not match type " + self.name)

		if self.enumeration is not None:
			if input not in self.enumeration:
				raise TypeError(input + " is not in the enumeration list for " + self.name)

	def writeXml(self, out, paramname, input, nscount):
		"""Writes the XML for this type to the output.  Returns the input
		nscount plus 1 since it uses this namespace."""
		out.write('<' + paramname + ' xmlns:q' + str(nscount) + '="' +
				self.namespace + '" type="q' +
				str(nscount) + ':' +
				self.name + '">' + str(input) +
				'</' + paramname + '>\n')
		return nscount + 1


	def translate(self, input):
		"""Translates the input to the appropriate Python type.  Input
		is a list."""
		
		f = self.trans_dispatch[self.base_namespace + self.base_type]
		return f(self,input)

	def translate_int(self, input):
		return int(string.join(input,""))
	trans_dispatch[NS_XSD + 'int'] = translate_int
	trans_dispatch[NS_XSD + 'byte'] = translate_int
	trans_dispatch[NS_XSD + 'unsignedByte'] = translate_int
	trans_dispatch[NS_XSD + 'short'] = translate_int
	trans_dispatch[NS_XSD + 'unsignedShort'] = translate_int
	trans_dispatch[NS_XSD + 'long'] = translate_int
	python_types[NS_XSD + 'int'] = 'int'
	python_types[NS_XSD + 'byte'] = 'int'
	python_types[NS_XSD + 'unsignedByte'] = 'int'
	python_types[NS_XSD + 'short'] = 'int'
	python_types[NS_XSD + 'unsignedShort'] = 'int'
	python_types[NS_XSD + 'long'] = 'int'

	def translate_str(self, input):
		return string.join(input,"")
	trans_dispatch[NS_XSD + 'string'] = translate_str
	python_types[NS_XSD + 'string'] = 'string'

	def translate_long(self, input):
		return long(string.join(input,""))
	trans_dispatch[NS_XSD + 'integer'] = translate_long
	trans_dispatch[NS_XSD + 'unsignedInt'] = translate_long
	trans_dispatch[NS_XSD + 'unsignedLong'] = translate_long
	python_types[NS_XSD + 'integer'] = 'long'
	python_types[NS_XSD + 'unsignedInt'] = 'long'
	python_types[NS_XSD + 'unsignedLong'] = 'long'

	def translate_float(self, input):
		return float(string.join(input,""))
	trans_dispatch[NS_XSD + 'double'] = translate_float
	trans_dispatch[NS_XSD + 'float'] = translate_float
	python_types[NS_XSD + 'double'] = 'float'
	python_types[NS_XSD + 'float'] = 'float'

import base64
class BinaryType(SimpleType):
	"""Represents a SOAP binary type"""

	def __init__(self, encoding='base64', namespace='http://www.w3.org/1999/XMLSchema', name='binary'):
		SimpleType.__init__(self, name, namespace)
		self.encoding = encoding

	def set_encoding(self, encoding):
		self.encoding = encoding

	def checkType(self, input):
		if type(input) != StringType:
			raise TypeError(str(input) + " is not a string and cannot be a binary object.")

	def translate(self, input):
		if self.encoding == 'base64':
			return base64.decodestring(string.join(input,""))

class BooleanType(SimpleType):
	"""Represents a SOAP boolean type"""

	def __init__(self, namespace='http://www.w3.org/1999/XMLSchema', name='boolean'):
		SimpleType.__init__(self, name, namespace)

	def checkType(self, input):
		"""For input, we only accept integers, either 1 or 0"""
		if input != 1 and input != 0:
			raise TypeError(str(input) + " must be either 1 or 0.")

	def translate(self, input):
		if type(input) is not TupleType:
			return not not input # force to a boolean
		else:
			return not not input[0]

class DateTimeType(SimpleType):
	"""Represents a SOAP date/time type"""

	def __init__(self, name, namespace='http://www.w3.org/1999/XMLSchema'):
		SimpleType.__init__(self, name, namespace)

	#FIXME: Implement this later
	def checkType(self, input):
		pass

	#FIXME: how should this work?
	def translate(self, input):
		return string.join(input,"")

SIMPLE_TYPES = {NS_XSD + 'int': SimpleType('int', NS_XSD),
				NS_XSD + 'byte': SimpleType('byte',NS_XSD),
				NS_XSD + 'unsignedByte': SimpleType('unsignedByte',NS_XSD),
				NS_XSD + 'short': SimpleType('short',NS_XSD),
				NS_XSD + 'unsignedShort': SimpleType('unsignedShort',NS_XSD),
				NS_XSD + 'long': SimpleType('long',NS_XSD),
				NS_XSD + 'string': SimpleType('string',NS_XSD),
				NS_XSD + 'integer': SimpleType('integer',NS_XSD),
				NS_XSD + 'unsignedInt': SimpleType('unsignedInt',NS_XSD),
				NS_XSD + 'unsignedLong': SimpleType('unsignedLong',NS_XSD),
				NS_XSD + 'double': SimpleType('double',NS_XSD),
				NS_XSD + 'float': SimpleType('float',NS_XSD),
				NS_XSD + 'binary': SimpleType('binary',NS_XSD),
				NS_XSD + 'boolean': BooleanType(),
				NS_XSD + 'timeInstant': DateTimeType('timeInstant'),
				NS_XSD + 'timePeriod': DateTimeType('timePeriod')}

class Method:
	"""
	Represents a SOAP/WSDL Method.  Includes the method name, the
	endpoint (i.e. the URL which this method calls to execute), the
	namespace for the method, the 'SOAP action' which should be sent
	in the HTTP 'SOAPAction:' header, and the name and type of all
	input and output parameters for the method.
	"""
	def __init__(self, name, endpoint, namespace="", soap_action=""):
		self.name = name
		self.endpoint = endpoint
		self.namespace = namespace
		self.soap_action = soap_action
		self.all_namespaces = NAMESPACES.copy()
		self.params = {}
		self.members = {}
		self.nscount = 0
		self.response_name = None

	def setResponseName(self, name):
		self.response_name = name

	def addParam(self, name, type):
		"Add an input parameter with the given parameter name and type"
		self.params[name] = type
		if not (self.all_namespaces.has_key(type.namespace)):
			self.all_namespaces[type.namespace] = "s%d" % (self.nscount)
			self.nscount = self.nscount + 1

	def addResult(self, name, type):
		"""Add an output parameter (result) with the given parameter name
		and type"""
		self.members[name] = type
		if not (self.all_namespaces.has_key(type.namespace)):
			self.all_namespaces[type.namespace] = "s%d" % (self.nscount)
			self.nscount = self.nscount + 1

	def checkParams(self, args):
		"""Check a dictionary containing argument names and values against
		the types listed for those input parameters.  Raises a TypeError
		exception if any of the types are not of the right type."""

		for k in args.keys():
			self.params[k].checkType(args[k]) # raises exception if not valid
		return 1

	def writeCallXml(self, out, args):
		"""Outputs XML to the output stream identified by 'out'
		representing a SOAP call to this method with the given
		dictionary containing argument names and values."""

		out.write('<?xml version="1.0"?>\n')
		out.write('<soap:Envelope')
		out.write(' soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"')
		for namespace in self.all_namespaces.keys():
			out.write(' xmlns:' + self.all_namespaces[namespace] +
					  '="' + namespace + '"')
		out.write('>\n')
		out.write('<soap:Body>\n')
		out.write('<' + self.name)
		if not (self.namespace is None):
			out.write(' xmlns="' + self.namespace + '"')
		out.write('>\n')
		nscount = 0
		for k in args.keys():
			paramtype = self.params[k]
			nscount = paramtype.writeXml(out,k[len(self.namespace):],args[k],nscount)
		out.write('</' + self.name + '>\n')
		out.write('</soap:Body>\n')
		out.write('</soap:Envelope>')

	def __call__(self, *pargs, **kwargs):
		buf = StringIO()
		fixedargs = {}
		for arg in kwargs.keys():
			fixedargs[self.namespace + arg] = kwargs[arg]
		self.checkParams(fixedargs)
		self.writeCallXml(buf,fixedargs)

		url = self.endpoint
		protocol, uri = urllib.splittype(url)
		host, uri_path = urllib.splithost(uri)
		request_body = buf.getvalue()
		#print "Sending",request_body
		
		response = request(protocol, host, uri_path, self.soap_action,
						   request_body)
		rhandler = ResponseHandler(self)
		parser = getparser(rhandler)
		responsetext = response.read()
		#print "reponse:",responsetext
		parser.feed(responsetext)
		parser.close()

		result = rhandler.getresult()
		if result.has_key('result'):
			return result['result']
		else:
			return result

class AnonType:
	"""Placeholder for methods for context"""

	def __init__(self):
		self.members = {}
		self.namespace = ''

class ResponseHandler:
	"""Parse SOAP response based on incoming XML events; uses SDL/WSDL
	information to figure out data structure."""

	def __init__(self, method):
		self._method = method
		self._data = []
		self._context = AnonType()
		self._context.members[method.response_name] = method
		self._contextstack = []
		self._contexttag = None
		self._contexttagstack = []
		self._result = {}
		self._resultstack = []

	def start(self, tag, attrs):
		#print "Tag:",tag,"attrs:",attrs
		self._data = []
		if (self._context.members.has_key(tag)):
			type = self._context.members[tag]
			if not isinstance(type,SimpleType):
				if not self._contexttag is None:
					newresult = {}
					key = tag[len(self._method.namespace):]
					if isinstance(type, ArrayType):
						if not self._result.has_key(key):
							self._result[key] = []
						self._result[key].append(newresult)
					else:
						self._result[key] = newresult
					self._resultstack.insert(0,self._result)
					self._result = newresult
				self._contextstack.insert(0,self._context)
				self._context = self._context.members[tag]
				self._contexttagstack.insert(0,self._contexttag)
				self._contexttag = tag

	def data(self, text):
		self._data.append(text)

	def end(self, tag):
		#print "end tag:",tag
		#print "et: self._contexttag:",self._contexttag
		#print "et: self._context:",self._context
		if tag == self._contexttag:
			self._context = self._contextstack[0]
			del self._contextstack[0]
			self._contexttag = self._contexttagstack[0]
			del self._contexttagstack[0]
			if (self._contexttag is not None):
				self._result = self._resultstack[0]
				del self._resultstack[0]
		elif self._context.members.has_key(tag):
			type = self._context.members[tag]
			if isinstance(type,SimpleType):
				self._result[tag[len(self._context.namespace):]] = type.translate(self._data)
			elif (isinstance(type, ArrayType) and
				  isinstance(type.actualtype, SimpleType)):
				key = tag[len(self._context.namespace):]
				if not self._result.has_key(key):
					self._result[key] = []
				self._result[key].append(type.actualtype.translate(self._data))

	def getresult(self):
		return self._result

SCHEMA_NAMESPACE="http://www.w3.org/1999/XMLSchema"
SDL_SOAP_NAMESPACE="urn:schemas-xmlsoap-org:soap-sdl-2000-01-25"
class SDLHandler:
	"""Loading class for Microsoft SDL documents.  This is old-style;
	newer services will use WSDL."""
	
	def __init__(self, proxy):
		self.proxy = proxy
		self.endpoint = None
		self.methods_input = {}
		self.methods_output = {}
		self.types = SIMPLE_TYPES.copy()
		self.elements = {}
		self.targetns = None

		self._currmethod = None
		self._currtype = ComplexType(None,None) # anonymous type
		self._currelement = None
		self._typestack = []
		self._elementstack = []
		
		self.in_soapmethods = 0
		self.in_schema = 0
		self._parser = None
		self._currnamespace = None

	_startmethods = {}
	_endmethods = {}

	def start(self, tag, attrs):
		ns = self._parser.getnamespace()['']
		if tag.count(ns) > 0:
			tag = tag[len(ns):]
		#print "start",tag,"attrs:",attrs

		if (self._startmethods.has_key(tag)):
			f = self._startmethods[tag]
			f(self, attrs)

	def data(self, text):
		pass

	def end(self, tag):
		ns = self._parser.getnamespace()['']
		if tag.count(ns) > 0:
			tag = tag[len(ns):]
		#print "end",tag

		if (self._endmethods.has_key(tag)):
			f = self._endmethods[tag]
			f(self)

	def start_serviceDescription(self, attrs):
		self._currnamespace = self._parser.getnamespace()['']
		ns = self._currnamespace
		self.targetns = attrs[ns + 'targetNamespace']
	_startmethods['serviceDescription'] = start_serviceDescription

	def start_soap(self, attrs):
		self._currnamespace = SDL_SOAP_NAMESPACE
		self.in_soapmethods = 1
	_startmethods['soap'] = start_soap

	def end_soap(self):
		self._currnamespace = self._parser.getnamespace()['']
		self.in_soapmethods = 0
	_endmethods['soap'] = end_soap
	
	def start_schema(self, attrs):
		self._currnamespace = SCHEMA_NAMESPACE
		self.in_schema = 1
	_startmethods['schema'] = start_schema

	def end_schema(self):
		self._currnamespace = self._parser.getnamespace()['']
		self.in_schema = 0
	_endmethods['schema'] = end_schema
	
	def start_address(self, attrs):
		ns = self._currnamespace
		if self.in_soapmethods:
			self.endpoint = attrs[ns + 'uri']
	_startmethods['address'] = start_address
	
	def start_requestResponse(self, attrs):
		ns = self._currnamespace
		if self.in_soapmethods:
			method = Method(attrs[ns + 'name'],
							self.endpoint,
							self.targetns,
							attrs[ns + 'soapAction'])
			self.proxy.addMethod(method)
			self._currmethod = method
	_startmethods['requestResponse'] = start_requestResponse

	def end_requestResponse(self):
		ns = self._currnamespace
		if (self._currmethod is not None):
			self._currmethod = None
	_endmethods['requestResponse'] = end_requestResponse

	def start_request(self, attrs):
		ns = self._currnamespace
		if self._currmethod is not None:
			method_input_name = attrs[ns + 'ref']
			if ":" in method_input_name:
				minns, miname = string.split(method_input_name, ":")
				method_input_name = self._parser.getnamespace()[minns] + miname
			if not self.methods_input.has_key(method_input_name):
				self.methods_input[method_input_name] = []
			self.methods_input[method_input_name].append(self._currmethod)
	_startmethods['request'] = start_request

	def start_response(self, attrs):
		ns = self._currnamespace
		if self._currmethod is not None:
			method_output_name = attrs[ns + 'ref']
			if ":" in method_output_name:
				monns, moname = string.split(method_output_name, ":")
				method_output_name = self._parser.getnamespace()[monns] + moname
			self._currmethod.setResponseName(method_output_name)
			if not self.methods_output.has_key(method_output_name):
				self.methods_output[method_output_name] = []
			self.methods_output[method_output_name].append(self._currmethod)
	_startmethods['response'] = start_response

	def start_simpleType(self, attrs):
		ns = self._currnamespace
		targetns = self.targetns

		name = None
		if (attrs.has_key(ns + 'name')):
			name = attrs[ns + 'name']
		basetype = attrs[ns + 'base']
		basetypens = ''
		if ":" in basetype:
			basetypens, basetype = string.split(basetype,":")
			basetypens = self._parser.getnamespace()[basetypens]
		if basetype == 'binary':
			type = BinaryType()
		else:
			type = SimpleType(name, targetns, basetype, basetypens)

		self._typestack.insert(0,self._currtype)
		self._currtype = type
		if name is not None:
			self.types[targetns + name] = type
	_startmethods['simpleType'] = start_simpleType

	def start_enumeration(self, attrs):
		ns = self._currnamespace
		self._currtype.addEnumElement(attrs[ns + 'value'])
	_startmethods['enumeration'] = start_enumeration

	def start_encoding(self, attrs):
		ns = self._currnamespace
		self._currtype.set_encoding(attrs[ns + 'value'])
	_startmethods['encoding'] = start_encoding

	def end_simpleType(self):
		type = self._currtype
		self._currtype = self._typestack[0]
		del self._typestack[0]
		if self._currelement is not None:
			self._currelement['type'] = type
	_endmethods['simpleType'] = end_simpleType

	def start_complexType(self, attrs):
		ns = self._currnamespace
		targetns = self.targetns
		name = None
		if (attrs.has_key(ns + 'name')):
			name = attrs[ns + 'name']
		type = ComplexType(name, targetns)
		self._typestack.insert(0,self._currtype)
		self._currtype = type
		if name is not None:
			self.types[targetns + name] = type
	_startmethods['complexType'] = start_complexType

	def end_complexType(self):
		type = self._currtype
		self._currtype = self._typestack[0]
		del self._typestack[0]
		if self._currelement is not None:
			self._currelement['type'] = type
	_endmethods['complexType'] = end_complexType

	def start_element(self, attrs):
		ns = self._currnamespace
		name = attrs[ns + 'name']
		self._elementstack.insert(0,self._currelement)
		self._currelement = {}
		self._currelement['name'] = name
		if attrs.has_key(ns + 'type'):  # this is a simple element
			type = attrs[ns + 'type']
			typens = ''
			if ":" in type:
				typens, type = string.split(type,":")
				typens = self._parser.getnamespace()[typens]
			if self.types.has_key(typens + type):
				self._currelement['type'] = self.types[typens + type]
			else:
				self._currelement['type'] = typens + type
		if attrs.has_key(ns + 'minOccurs'): # this is an array element
			self._currelement['minOccurs'] = attrs[ns + 'minOccurs']
			self._currelement['maxOccurs'] = attrs[ns + 'maxOccurs']

	_startmethods['element'] = start_element

	def end_element(self):
		type = self._currelement['type']
		if self._currelement.has_key('minOccurs'):
			minOccurs = int(self._currelement['minOccurs'])
			maxOccurs = None
			if (self._currelement['maxOccurs'] != 'unbounded'):
				maxOccurs = int(self._currelement['maxOccurs'])
			type = ArrayType(type, minOccurs, maxOccurs)
		self._currtype.addMember(self.targetns, self._currelement['name'], type)
		self._currelement = self._elementstack[0]
		del self._elementstack[0]
	_endmethods['element'] = end_element
	
	def close(self):
		for key in self.methods_input.keys():
			for method in self.methods_input[key]:
				element = self._currtype.members[key]
				self.fixtypes(element.members)
				for param in element.members.keys():
					paramtype = element.members[param]
					method.addParam(param,paramtype)
		for key in self.methods_output.keys():
			for method in self.methods_output[key]:
				element = self._currtype.members[key]
				self.fixtypes(element.members)
				for param in element.members.keys():
					paramtype = element.members[param]
					method.addResult(param,paramtype)
		self.proxy.types = self.types
		#print "methods_input:",self.methods_input
		#print "methods_output:",self.methods_output
		#print "types",self.types
		#print "input/output elements",self._currtype.members

	def fixtypes(self, members):
		for param in members.keys():
			if type(members[param]) is StringType:
				members[param] = self.types[members[param]]
			if (isinstance(members[param],ArrayType) and
				  type(members[param].actualtype) is StringType):
				members[param].setType(self.types[members[param].actualtype])
			if (isinstance(members[param], ComplexType)):
				self.fixtypes(members[param].members)
				
	
class WSDLHandler:
	"""Loading class for WSDL service description documents."""
	
	def __init__(self, proxy):
		self.proxy = proxy
		self.endpoint = None
		self.methods_input = {}
		self.methods_output = {}
		self.types = {}

	def start(self, tag, attrs, getnamespace):
		print "Tag:",tag

	def data(self, text):
		pass

	def end(self, tag, getnamespace):
		pass
	

class SOAPProxy:
	def __init__(self, uri, filetype='wsdl'):
		self.methods = {}
		self.types = None
		sdlhandler = None
		if (filetype == 'sdl'):
			sdlhandler = SDLHandler(self)
		elif (filetype == 'wsdl'):
			sdlhandler = WSDLHandler(self)
		parser = getparser(sdlhandler)
		file = urllib.urlopen(uri)
		parser.feed(file.read())
		parser.close()
		sdlhandler.close()

	def __getattr__(self, name):
		# Magic to make methods appear like real methods
		return self.methods[name]

	def addMethod(self, method):
		self.methods[method.name] = method



# --------------------------------------------------------------------
# XML parsers
# Taken from soaplib.py under the following license conditions:
# The soaplib.py library is
# 
# Copyright (c) 1999-2000 by Secret Labs AB
# Copyright (c) 1999-2000 by Fredrik Lundh
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# Secret Labs AB or the author not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.
#
# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
# TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANT-
# ABILITY AND FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR
# BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
# DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
# ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
# OF THIS SOFTWARE.
# --------------------------------------------------------------------
class SlowParser(xmllib.XMLParser):
	# slow but safe standard parser, based on the XML parser in
	# Python's standard library

	def __init__(self, target):
		self.__start = target.start
		self.handle_data = target.data
		self.__end = target.end
		target._parser = self
		xmllib.XMLParser.__init__(self)

	def unknown_starttag(self, tag, attrs):
		# fixup tags and attribute names (ouch!)
		tag = string.replace(tag, " ", "")
		for k, v in attrs.items():
			k = string.replace(k, " ", "")
			attrs[k] = v
		self.__start(tag, attrs)

	def unknown_endtag(self, tag):
		tag = string.replace(tag, " ", "")
		self.__end(tag)

# Utility method
def getparser(target):
	"""get the fastest available parser, and attach it to the given
	unmarshalling object."""
	return SlowParser(target)



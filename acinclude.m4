## this one is commonly used with AM_PATH_PYTHONDIR ...
dnl AM_CHECK_PYMOD(MODNAME [,SYMBOL [,ACTION-IF-FOUND [,ACTION-IF-NOT-FOUND]]])
dnl Check if a module containing a given symbol is visible to python.
AC_DEFUN(AM_CHECK_PYMOD,
[AC_REQUIRE([AM_PATH_PYTHON])
py_mod_var=`echo $1['_']$2 | sed 'y%./+-%__p_%'`
AC_MSG_CHECKING(for ifelse([$2],[],,[$2 in ])python module $1)
AC_CACHE_VAL(py_cv_mod_$py_mod_var, [
ifelse([$2],[], [prog="
import sys
try:
        import $1
except ImportError:
        sys.exit(1)
except:
        sys.exit(0)
sys.exit(0)"], [prog="
import $1
$1.$2"])
if $PYTHON -c "$prog" 1>&AC_FD_CC 2>&AC_FD_CC
  then
    eval "py_cv_mod_$py_mod_var=yes"
  else
    eval "py_cv_mod_$py_mod_var=no"
  fi
])
py_val=`eval "echo \`echo '$py_cv_mod_'$py_mod_var\`"`
if test "x$py_val" != xno; then
  AC_MSG_RESULT(yes)
  ifelse([$3], [],, [$3
])dnl
else
  AC_MSG_RESULT(no)
  ifelse([$4], [],, [$4
])dnl
fi
])

dnl a macro to check for ability to create python extensions
dnl  AM_CHECK_PYTHON_HEADERS([ACTION-IF-POSSIBLE], [ACTION-IF-NOT-POSSIBLE])
dnl function also defines PYTHON_INCLUDES
AC_DEFUN([AM_CHECK_PYTHON_HEADERS],
[AC_REQUIRE([AM_PATH_PYTHON])
AC_MSG_CHECKING(for headers required to compile python extensions)
dnl deduce PYTHON_INCLUDES
py_prefix=`$PYTHON -c "import sys; print sys.prefix"`
py_exec_prefix=`$PYTHON -c "import sys; print sys.exec_prefix"`
PYTHON_INCLUDES="-I${py_prefix}/include/python${PYTHON_VERSION}"
if test "$py_prefix" != "$py_exec_prefix"; then
  PYTHON_INCLUDES="$PYTHON_INCLUDES -I${py_exec_prefix}/include/python${PYTHON_VERSION}"
fi
AC_SUBST(PYTHON_INCLUDES)
dnl check if the headers exist:
save_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$CPPFLAGS $PYTHON_INCLUDES"
AC_TRY_CPP([#include <Python.h>],dnl
[AC_MSG_RESULT(found)
$1],dnl
[AC_MSG_RESULT(not found)
$2])
CPPFLAGS="$save_CPPFLAGS"
])



dnl This macro defines the PYTHON_EMBED_{CFLAGS,LDFLAGS,LIBS} substitutions
dnl that should be used when embedding the python interpreter into a program.
dnl AM_INIT_PYTHON_EMBED
AC_DEFUN(AM_INIT_PYTHON_EMBED,
[AC_REQUIRE([AM_PATH_PYTHON])
AC_REQUIRE([AM_CHECK_PYTHON_HEADERS])

AC_MSG_CHECKING(for flags used to embed python interpreter)
changequote(,)dnl
py_makefile="`$PYTHON -c '
import sys
print \"%s/lib/python%s/config/Makefile\"%(sys.exec_prefix, sys.version[:3])'`"
changequote([,])dnl
if test ! -f "$py_makefile"; then
   AC_MSG_ERROR([*** Couldn't find the python config makefile.  Maybe you are
*** missing the development portion of the python installation])
fi

changequote(,)dnl
py_lib="`$PYTHON -c '
import sys
ver = sys.version[:3]
pre = sys.exec_prefix
print \"-L%s/lib/python%s/config\" % (pre, ver),
if ver == \"1.4\":
	print \"-lPython -lObjects -lParser\"
else:
	print \"-lpython\" + ver
'`"
changequote([,])dnl

py_ldflags=`sed -n -e 's/^LDFLAGS=\(.*\)/\1/p' $py_makefile`
py_linkforshared=`sed -n -e 's/^LINKFORSHARED=\(.*\)/\1/p' $py_makefile`

PYTHON_EMBED_LDFLAGS="$py_ldflags $py_linkforshared"

py_localmodlibs=`sed -n -e 's/^LOCALMODLIBS=\(.*\)/\1/p' $py_makefile`
py_basemodlibs=`sed -n -e 's/^BASEMODLIBS=\(.*\)/\1/p' $py_makefile`
py_other_libs=`sed -n -e 's/^LIBS=\(.*\)/\1/p' $py_makefile`

PYTHON_EMBED_LIBS="$py_lib $py_localmodlibs $py_basemodlibs $py_other_libs"

PYTHON_EMBED_CFLAGS="$PYTHON_INCLUDES"

AC_MSG_RESULT(done)

AC_SUBST(PYTHON_EMBED_CFLAGS)
AC_SUBST(PYTHON_EMBED_LDFLAGS)
AC_SUBST(PYTHON_EMBED_LIBS)
])

dnl creates a config.c file, which holds the builtin module initialisation
dnl table for python.  The first argument should be the output filename.
dnl The second argument gives the names of all the modules you want to build
dnl into the executable.
dnl AM_PYTHON_CREATE_CONFIG_C(CONFIG_C, MODULE ...)
AC_DEFUN(AM_PYTHON_CREATE_CONFIG_C,
[AC_REQUIRE([AM_INIT_PYTHON_EMBED])
AC_MSG_CHECKING(for config.c.in)
changequote(,)dnl
py_config_in="`$PYTHON -c '
import sys
print \"%s/lib/python%s/config/config.c.in\"%(sys.exec_prefix, sys.version[:3])'`"
changequote([,])dnl
if test ! -f "$py_config_in"; then
   AC_MSG_ERROR([*** Couldn't find the config.c.in file.  Maybe you are
*** missing the development portion of the python installation])
fi

py_cnf_decls=""
py_cnf_inits=""
py_nl='\
'
for mod in . $2; do
  if test "$mod" != .; then
    py_cnf_decls="${py_cnf_decls}extern void init$mod();$py_nl"
    py_cnf_inits="${py_cnf_inits}  {\"$mod\", init$mod},$py_nl"
  fi
done
sed -e "
  /MARKER 1/i$py_nl$py_cnf_decls
  /MARKER 2/i$py_nl$py_cnf_inits
" $py_config_in > $1
AC_MSG_RESULT(created $1)
])
